## bcamsc

**bcamsc** is a Python 3.6 module for machine learning build on top of scikit-learn and cvxpy and distributed under the BCAM license.

bcamsc implement some supervised classification algorithms based on the scikit-learn api conventions. The goal of the library is to
help bcam researchs team to compare their own algorithms to some others provided by the libray.

The library is composed of some datasets and some supervised classification algorithms.

---

## Datasets

- **Adult**
- **column_3C**
- **credit**
- **diabetes**
- **glass**
- **haberman**
- **indianLiverPatient**
- **magic**
- **mammographic**
- **satellite**
- **vehicle**
- **synthetic**

---

## Algorithms

- **Adversarial Cost Sensitive Classification**

"Adversarial Cost-Sensitive Classification".
Kaiser, A. Wei, X. Sima, B. and Brian, D.Z.
publish in: Proceeding UAI'15 Proceedings of the Thirty-First Conference
on Uncertainty in Artificial Intelligence, Pages 92-101 
http://auai.org/uai2015/proceedings/papers/33.pdf

- **Learning Using Statistical Invariants**: 

"Rethinking statistical learning theory: learning using statistical invariants".
Vapnik, V. and Izmailov, R.
Machine Learning. https://link.springer.com/article/10.1007%2Fs10994-018-5742-0

- **Maximum Entropy Machine**: 

"A Minimax Approach to Supervised Learning". Farzan, F. and David, T.
arXiv:1606.02206v5 [stat.ML]. 4 Jul 2017
https://papers.nips.cc/paper/6247-a-minimax-approach-to-supervised-learning.pdf

- **Minimax Probabilitistic Transformation_v1**

Implementation from Andrea Zanoni code.

- **Minimax Probabilitistic Transformation_v2**

Implementation based on the file 2019_Supervised classification based on minimax probabilistic transformation_pseudo_codigo.pptx
This implementation can used a phi function that correspond to the 2nd order moment 
or the first order moment.

- **Minimax Probabilitistic Transformation_v3**

Implementation made from a Santi matlab version.
Phi is compound of classifiers. For the moment just the equality
case is implemented with all the optimization constraints compute at the
same time. The phi function is created with 3 classifiers choosen between
the one that exist in scikit learn.

- **Minimax Probabilitistic Transformation_v4** 

The same as the previous one but the classifiers 
are fixed (svm, random forest, decision tree). We can just change some 
paremeters of these classifiers in order to perform a gridsearch.

---

## Install an anaconda environnement to work on the libray from scratch

clone the bcamsc library: 

**$ git clone https://GuiBCAM@bitbucket.org/GuiBCAM/bcamsc.git bcamsc**

This will copy the bitbucket repository to the bcamsc folder.

After having installed Anaconda, activate it:

**$ source .bashrc**

Create a new python3.6 conda environnement called bcamsc:

**$ conda create --name bcamsc python=3.6**

Activate the conda bcamsc new environnement:

**$ conda activate bcamsc**

Then install the following packages:

**$ conda install pip** (if you need to install some packege with pip, and particularly the bcamsc library).

**$ conda install -c conda-forge scs lapack glpk -y**

**$ conda install -c cvxgrp cvxopt cvxpy -y**

**$ conda install nose** (in order to test the cvxpy installation)

**$ conda install scikit-learn matplotlib pandas**

To test cvxpy installation run:

**$ nosetests cvxpy**

---

##Install the bcamsc last dev version inside bcamsc conda environnement

1. move to bcamsc folder **$ cd bcamsc**

2. Create the file .whl: **$ python -m setup sdist bdist_wheel**. 
This will create the folder **dist** with the file 
**bcamsc-0.0.dev6-py3-none-any.whl** in it.

3. Installed the library from .whl file with pip: 
**pip install dist/bcamsc-0.0.dev6-py3-none-any.whl**

You can now use the library directly inside the bcamsc env from everywhere in 
the computer like every python library.

---

##Run the simulations on the cluster

1. Connect to hipatia cluster: **$ ssh -p 6556 username@bcamath.org** and enter your password.
The command prompt will change to: **username@login01:~$**

2. Install anaconda and bcamsc library has explained upstaires.

3. move to bcamsc/results folder: **username@login01:~$cd bcamsc/results**

4. run the bash script: **username@login01:~/bcamsc/results$ bash gridsearch.sh**

- To modify the datasets and classifiers used in the gridsearch, modify the **gridsearch.sh** script.

- To modify the slurm command, modify the **gridsearch.sl** script. 
In this script we also told to slurm to activate our bcamsc conda environnement 
with **source $HOME/anaconda3/bin/activate bcamsc**.
By this way we can used bcamsc environnement on the cluster working node, 
but as anaconda has been installed on your home node, the installed python 
libraries will not be linked directly with the working node C or mkl 
libraries, so we will not use the full possibilities of the working node.

---

##Work on the project on Eclipse

1. Creata a new eclipse project
2. Add a new python interpreter in order to use the bcamsc conda environnement 
when you run the script inside eclipse. In **Window** go to **Preferences**.
A new windows appears.
Go to **PyDev --> Interpreters --> Python Interpreter** on the left go to **Browse for python/pydev exe**.
This opens the **select interpreter** window. 
Give a name to you interpreter, for exemple *bcamsc*. in the **interpreter executable** 
use the browser to indicate the bcamsc interpreter **...envs/bcamsc/bin/python3.6**