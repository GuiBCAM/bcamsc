# -*- coding: utf-8 -*-


# Authors: Guillaume Gelabert
#
# License: ?
#
# Nested grid search. This module work for the bcamsc library installed. If
# it's not the case, one have to modify the import statement reemplacing
# bcamsc by bcamsc.bcamsc

import os
import errno

import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
import pickle as pkl
import pandas as pd

from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.tree import DecisionTreeClassifier
from sklearn.gaussian_process.gpc import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import (RBF, Matern, RationalQuadratic,
                                              ExpSineSquared, DotProduct,
                                              ConstantKernel)

from sklearn.preprocessing import StandardScaler
from sklearn.impute import SimpleImputer
from sklearn.pipeline import Pipeline
from sklearn.model_selection import StratifiedShuffleSplit, GridSearchCV
from sklearn.model_selection import cross_val_score

from bcamsc.AdversarialCostSensitiveClassification \
    import CostSensitiveLearning as CSL
from bcamsc.MinimaxProbabilisticTransformation_v1 \
    import MinimaxProbTransformation as MPT
from bcamsc.MinimaxProbabilisticTransformation_v4 \
    import Algo as MPT4
from bcamsc.LearningUsingStatisticalInvariants import lusi
from bcamsc.MaximumEntropyMachine import Mem

from globals import dirname, just_plot, seed, data_list, inner_cv, outer_cv

def make_sure_path_exists(path):
    """Verify that the given path exist, if not create it.

    Parameters
    ----------
    path: string. 

    """
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

def scores_save(scores, name, load, dirname=dirname):
    '''Save the scores as a pickle object named name.pkl
    The score is saved at the following file path: 
    current_dir/nested_cv/datasets/dataset_name/name.pkl

    Parameters
    ----------
    scores: array-like, shape (n_splits, )

    name: string
        The name of the estimator used in the nested cross validation.

    load: load function.
        This is assumed to be a scikit-learn load function. Call load with the
        keyword argument return_X_y will return a tuple (X, y).

    dirname: string, default 'nested_cv'
        The score will be store in at the following path:
        "current_dir/dirname/datasets/load.__name__/name.pkl"

    '''
    filename = '.'.join([name,'pkl'])
    path = os.path.join(os.getcwd(), dirname, 'datasets', 
                       str(load.__name__.split('_')[-1]))
    make_sure_path_exists(path)
    filepath = os.path.join(path, filename)
    with open(filepath, mode='wb') as f:
        pkl.dump(scores, f)

def score_yet_calculated(name, load, dirname=dirname):
    '''Indicate if the file:
    current_dir/dirname/datasets/dataset_name/name.pkl exist.
    
    Parameters
    ----------
    scores: array-like, shape (n_splits, )

    name: string
        The name of the estimator used in the nested cross validation.

    load: load function.
        This is assumed to be a scikit-learn load function. Call load with the
        keyword argument return_X_y will return a tuple (X, y).

    dirname: string, default 'nested_cv'
        The score will be store in at the following path:
        "current_dir/dirname/datasets/load.__name__/name.pkl"

    Returns
    -------
    path_exist: Bool
        Return True if the file exist.

    filepath: String
        The filepath.

    '''
    filename = '.'.join([name,'pkl'])
    path = os.path.join(os.getcwd(), dirname, 'datasets', 
                       str(load.__name__.split('_')[-1]))
    filepath = os.path.join(path, filename)
    path_exist = os.path.exists(filepath)
    return path_exist, filepath

def get_cls_param_grid(random_state=seed):
    """Create a list of tuple (classifier, parameter grid) that can be used
    as GridSearcCV arguments.

    Parameters
    ----------
    random_state : int, RandomState instance or None, optional, default 42
        If int, random_state is the seed used by the random number generator;
        If RandomState instance, random_state is the random number generator;
        If None, the random number generator is the RandomState instance used
        by np.random.

    Returns
    -------
    classifiers: dict of tuple (cls, param_grid)
        dict's keys are the following:
        ['Mem', 'Csl', 'Mpt', 'Lusi', 'Svc', 'RndF', 'Knn', 'Mpt4', 'AdaB',
         'DcT', 'Qda', 'Gpc]
        cls is assumed to implement the scikit-learn estimator interface.
        Either estimator needs to provide a ``score`` function,
        or ``scoring`` must be passed.

        param_grid: see nested_gridsearch()
        The tuple can be used to create a GridSearchCV instance.

    """
    classifiers = dict()
    # ================= Maximum Entropy Machine =========================
    cls = Mem(random_state=random_state) 

    pm_stochastic = dict(
        lambd=(np.ravel(np.logspace(-10, 0, 11, 
                                    dtype=np.float64)[:,np.newaxis]
                        * np.linspace(1., 5., 2, dtype=np.float64))),
        learning_rate_init=[0.01, 0.1, 1, 2, 5, 10],
        lr_schedule=['invscaling', 'constant'],
        max_iter=[1, 3, 5, 10]
        )

    pm_cvx = dict(
        lambd=(np.ravel(np.logspace(-10, 0, 11, dtype=np.float64)[:,np.newaxis]
                        * np.linspace(1., 5., 2, dtype=np.float64))),
        solver=['cvx']
        )

    pm = [pm_cvx, pm_stochastic]

    classifiers['Mem'] = (cls, pm)

    # ================= Cost Sensitive Learning classifier ==============
    cls = CSL(early_stopping=False,
              random_state=random_state,
              verbose=False) 

    pm = dict(
        learning_rate_init=[0.01, 0.1, 1, 2, 5, 10],
        lr_schedule=['invscaling', 'constant'],
        max_iter=[1, 3, 5, 10],
        deterministic = [True, False],
        inequality = [True, False]
        )

    classifiers['Csl'] = (cls, pm)

    # ================= Minimax Probabilisitc Transformation =============
    cls = MPT(random_state=random_state)

    pm = dict(
        deterministic = [True, False])

    classifiers['Mpt'] = (cls, pm)

    # ================= Learning Using Statistical Invariant =============
    cls = lusi()

    pm = dict(
        gamma = np.logspace(-10, 5, num = 16),
        delta = np.logspace(-10, 5, num = 16)
        )

    classifiers['Lusi'] = (cls, pm)

    # ================= SVM ===============================================
    cls = SVC(kernel="rbf",
              random_state=random_state)

    pm = dict(
        gamma = np.logspace(-10, 5, num = 16),
        C = np.logspace(-10, 5, num = 16),
        )

    classifiers['Svc'] = (cls, pm)

    # ================= RandomForest ======================================
    cls = RandomForestClassifier(random_state=random_state,
                                 n_jobs=1)
    # define parameters grid
    pm = dict(
        n_estimators = [100, 500, 1000],
        )
    classifiers['RandF'] = (cls, pm)

    # ================= KNeighborsClassifier ===============================
    cls = KNeighborsClassifier(algorithm='kd_tree',
                               n_jobs=1)

    pm = dict(
        n_neighbors=[1, 3, 5, 7],
        )

    classifiers['Knn'] = (cls, pm)

    # ================= Minimax Probabilistic Transformation v4 ===========
    cls = MPT4(random_state=random_state)
    # define parameters grid
    svc_C=np.logspace(-10, 5, num = 16)[::2]
    knn_n_neighbors=[1, 3, 5]
    rf_n_estimators=[100, 500, 1000]

    pm = dict(
        deterministic=[True, False],
        svc_C=svc_C,
        knn_n_neighbors=knn_n_neighbors,
        rf_n_estimators=rf_n_estimators
        )

    classifiers['Mpt4'] = (cls, pm)

    # ================= AdaBoostClassifier ================================
    cls = AdaBoostClassifier(random_state=random_state)

    pm = dict(
        n_estimators=[50, 100, 500, 1000],
        )

    classifiers['AdaB'] = (cls, pm)

    # ================= QuadraticDiscriminantAnalysis =====================
    cls = QuadraticDiscriminantAnalysis()
    classifiers['Qda'] = (cls, {})

    # ================= DecisionTreeClassifier ============================
    cls = DecisionTreeClassifier(random_state=random_state)

    pm = dict(
        min_samples_split=range(10,500,20),
        max_depth=range(1,20,2)
        )

    classifiers['DcT'] = (cls, {})

    # ================= GaussianProcessClassifier =========================
    cls = GaussianProcessClassifier(random_state=random_state, n_jobs=1)

    kernels = [1.0 * RBF(length_scale=1.0, length_scale_bounds=(1e-1, 10.0)),
               1.0 * RationalQuadratic(length_scale=1.0, alpha=0.1),
               1.0 * ExpSineSquared(length_scale=1.0, periodicity=3.0,
                                    length_scale_bounds=(0.1, 10.0),
                                    periodicity_bounds=(1.0, 10.0)),
                                    ConstantKernel(0.1, (0.01, 10.0))
               * (DotProduct(sigma_0=1.0, sigma_0_bounds=(0.1, 10.0)) ** 2),
               1.0 * Matern(length_scale=1.0,
                            length_scale_bounds=(1e-1, 10.0),
                            nu=1.5)]
    pm = dict(
        kernel = kernels
        )

    classifiers['Gpc'] = (cls, pm)

    return classifiers

def nested_gridsearch(estimator, parameters, load, inner_cv=inner_cv,
                      outer_cv=outer_cv, n_jobs=-1, random_state=seed):
    """Perform the nested gridsearch for the given estimtor and parameters
    grid.

    Parameters
    ----------
    estimator : estimator object.
        This is assumed to implement the scikit-learn estimator interface.
        Either estimator needs to provide a ``score`` function,
        or ``scoring`` must be passed.

    param_grid : dict or list of dictionaries
        Dictionary with parameters names (string) as keys and lists of
        parameter settings to try as values, or a list of such
        dictionaries, in which case the grids spanned by each dictionary
        in the list are explored. This enables searching over any sequence
        of parameter settings.

    load: load function.
        This is assumed to be a scikit-learn load function. Call load with the
        keyword argument return_X_y will return a tuple (X, y).

    inner_cv : int, cross-validation generator or an iterable, optional
        Determines the inner cross-validation splitting strategy, the one used
        by the gridsearch inside the outer cross-validation.
        Possible inputs for cv are:

        - None, to use the default 3-fold cross validation,
        - integer, to specify the number of folds in a `(Stratified)KFold`,
        - :term:`CV splitter`,
        - An iterable yielding (train, test) splits as arrays of indices.

        For integer/None inputs, if the estimator is a classifier and ``y`` is
        either binary or multiclass, :class:`StratifiedKFold` is used. In all
        other cases, :class:`KFold` is used.

        Refer :ref:`User Guide <cross_validation>` for the various
        cross-validation strategies that can be used here.

        .. versionchanged:: 0.20
            ``cv`` default value if None will change from 3-fold to 5-fold
            in v0.22.


    outer_cv : int, cross-validation generator or an iterable, optional
        Determines the outer cross-validation splitting strategy, the one used
        to cross-validate the gridsearch estimator.
        Possible inputs for cv are:

        - None, to use the default 3-fold cross validation,
        - integer, to specify the number of folds in a `(Stratified)KFold`,
        - :term:`CV splitter`,
        - An iterable yielding (train, test) splits as arrays of indices.

        For integer/None inputs, if the estimator is a classifier and ``y`` is
        either binary or multiclass, :class:`StratifiedKFold` is used. In all
        other cases, :class:`KFold` is used.

        Refer :ref:`User Guide <cross_validation>` for the various
        cross-validation strategies that can be used here.

        .. versionchanged:: 0.20
            ``cv`` default value if None will change from 3-fold to 5-fold
            in v0.22.

    n_jobs : int or None, optional (default=None)
        Number of jobs to run in parallel.
        ``None`` means 1 unless in a :obj:`joblib.parallel_backend` context.
        ``-1`` means using all processors. See :term:`Glossary <n_jobs>`
        for more details.

    random_state : int, RandomState instance or None, optional, default seed
        If int, random_state is the seed used by the random number generator;
        If RandomState instance, random_state is the random number generator;
        If None, the random number generator is the RandomState instance used
        by np.random.

    Returns
    -------
    nested_scores: array-like, shape (10,)
        The 10 fold cross validation scores return by the best estimator.
        This estimator is return by the gridsearch and fitted on all the
        dataset subset used for doing the gridsearch.

    """
    X, y = load(return_X_y = True)
    sci = SimpleImputer(missing_values=np.nan, strategy='mean')
    scl = StandardScaler()
    estimator_name = type(estimator).__name__
    pipe = Pipeline([('StandardScaler', scl), 
                     ('SimpleImputer', sci),
                     (estimator_name, estimator)])

    #create parameters
    pm_list = list()
    if type(parameters) != list:
        parameters = [parameters]
        
    for pm in parameters:
        pm_ = dict()
        for k,v in pm.items():
            pm_[estimator_name + '__' + k] = v
        pm_list.append(pm_)

    clf = GridSearchCV(pipe, pm_list, cv=inner_cv, n_jobs=n_jobs,
                       refit=True,
                       pre_dispatch=n_jobs * 2,
                       scoring='accuracy',
                       error_score=np.nan,
                       verbose=0)

    nested_scores = cross_val_score(clf, X=X, y=y, cv=outer_cv, n_jobs=n_jobs)

    return nested_scores

def read_score(cls, load):
    """Read the mean and std scores obtained from a saved file if the file
    exists. Function used in the function get_results((cls_list, data_list)

    Parameters
    ----------
    cls : estimator object.
        This is assumed to implement the scikit-learn estimator interface.
        Either estimator needs to provide a ``score`` function,
        or ``scoring`` must be passed.

    load: load function.
        This is assumed to be a scikit-learn load function. Call load with the
        keyword argument return_X_y will return a tuple (X, y).

    Returns
    -------
    tuple: (float, float)
        The mean and std of the scores obtained after the nested gridsearch of cls
        on the load dataset. If the score is not yet calculated,
        return (np.nan, np.nan)

    """
    cls_name = cls.__class__.__name__
    verif, filename = score_yet_calculated(cls_name, load)
    if verif:
        with open(filename, mode='rb') as f:
            return pkl.load(f)
    else:
        return np.array([np.nan])

def get_results(cls_list, data_list):
    """ Create a dictionary with estimator name as keys and a dict as value.
    The value dict has 2 keys, 'mean' and 'std', each one as a list as value.
    These lists are the results of the nested cross validation for each
    dataset.

    Parameters
    ----------
    cls_list: list of class.
        The classes that correspond to each algorithms.

    data_list: list of functions.
        The functions that load each necessary datasets.

    Returns
    -------
    results: dict, {...'Algorithm class name': {'mean':[], 'std':[]} ....}

    """
    results = {cls.__class__.__name__:{'mean':[], 'std':[]} 
               for cls in cls_list}

    for cls in cls_list:

        name = cls.__class__.__name__

        for load in data_list:
            scores = read_score(cls, load)
            results[name]['mean'].append(scores.mean())
            results[name]['std'].append(scores.std())
    return results

def plot_results_on_one_graph(results, data_list):

    x = list(map(lambda l:l.__name__, data_list))

    fig, ax = plt.subplots(1, figsize=(30,10))

    for cls_name, res in results.items():
        ax.plot(x, res['mean'], label=cls_name)
    ax.legend(loc='best')
    ax.set_xlabel('dataset name')
    ax.set_ylabel('accuracy in %')
    ax.grid()

def plot_bar(cls_list, loader, i, j):
    """Create a sub bar plot of the results of the classfifiers for one
    dataset.

    Parameters
    ----------
    cls_list: list of class.
        The classes that correspond to each algorithms.

    loader: dataset loader

    """
    res = get_results(cls_list, [loader])

    # If we want the real classifier names use in spite of the line below:
    # cls_names = list(res.keys())

    # Classifiers short names define in the function get_cls_param_grid
    cls_names = get_cls_param_grid().keys()

    # Obtain the datasets names
    data_name = loader.__name__.split('_')[-1]

    # Create the data to plot, the crossvalidation mean and std
    mean = [m[0] for m in map(lambda m:m['mean'], res.values())]
    std = [m[0] for m in map(lambda m:m['std'], res.values())]

    # Sort the mean and std for graph creation
    cls_names, mean, std = tuple(zip(*sorted(zip(cls_names, mean, std),
                                             key=lambda t:t[1],
                                             reverse=True)))
    mean, std = np.array(mean), np.array(std)

    # The number of classifiers used in the gridsearch
    N = len(mean)

    # The x axis of the graph
    x = np.arange(N)

    # select the indices where there is results for graph axis creation
    indices = np.nonzero(~np.isnan(mean))
    # Don't plot if there is no data
    if indices[0].size == 0:
        pass
    else:
        y_min = np.min(mean[indices]- std[indices]) - 0.005
        y_max = np.max(mean[indices]+ std[indices]) + 0.005

        #fig, ax = plt.subplots(num=i+1, figsize=(10,5))
        #locs, labels = plt.xticks(x, tuple(cls_names), rotation=90)
        ax[i, j].xaxis.set_ticks(x)
        ax[i, j].xaxis.set_major_formatter(
                                        ticker.FormatStrFormatter('%0.05f'))
        ax[i, j].set_xticklabels(
                cls_names,
                fontdict={'horizontalalignment':'center'},
                rotation=90,
                minor=False)
        ax[i, j].bar(x, height=mean, yerr=std)
        #ax[i, j].set_xlabel('classifier')
        #ax[i, j].set_ylabel('accuracy')
        ax[i, j].set_title(data_name)
        ax[i, j].axis([-0.5, N-0.5, y_min, y_max])

def save_res_as_csv(cls_list, rd=2):
    """Save the results for every datasets and the classfiers in cls_list as
    as an array in csv format.

    Parameters
    ----------
    cls_list: list of class.
        The classes that correspond to each algorithms.

    rd: int.
        Round the results to the indicated value. If rd=3, 43.567 --> 43.6

    """
    # =================== Create a table of the results ==================
    table = np.empty((len(cls_list), len(data_list)))
    # get a list of the classifiers names
    names = get_cls_param_grid().keys()
    datas = list(map(lambda f:f.__name__.split('_')[-1], data_list))
    for i, loader in enumerate(data_list):
        res = get_results(cls_list, [loader])
        means = [m[0]*100. for m in map(lambda m:m['mean'], res.values())]
        table[:, i] = means
    res_frame = pd.DataFrame(table, index=names, columns=datas)
    res_frame = res_frame.round(rd)
    array_path = os.path.join(dirname, 'res_array.csv')
    res_frame.to_csv(open(array_path, 'wt'))

if __name__ == "__main__":

    # The list of tuples (classificator, parameter grid) as define by the 
    # function get_cls_param_grid()
    cls_param_list = get_cls_param_grid().values()

    # just_plot is a global paramter define in global module.
    if just_plot:
        pass
    else:
        # Run for each dataset and tuple the nested cross-validation
        # data_list is define in global module.
        for (cls, pm) in list(cls_param_list):
            for load in data_list:

                cls_name = cls.__class__.__name__
                dataset_name = load.__name__

                print('\n{} on {} dataset\n'.format(cls_name, dataset_name))

                if score_yet_calculated(cls_name, load)[0]:
                    print('scores calculated yet')
                    pass 
                else:
                    scores = nested_gridsearch(cls, pm, load,
                                               n_jobs=2, random_state=seed)
                    print('{:^10}: {:0.5f} +- {:0.4f}'.format('best_score', 
                                                              scores.mean(),
                                                              scores.std()))
                    scores_save(scores, cls_name, load)

    # Create the results from saved scores and plot them
    cls_list = list(zip(*cls_param_list))[0]
    res = get_results(cls_list, data_list)

    plot_results_on_one_graph(res, data_list)


    # Save the results as an array in csv format --> res_array.csv
    save_res_as_csv(cls_list, 2)

    # Create the bar plot
    # Define the number of subplots per cols/rows. To determine in funcion of
    # datasets numbers
    fig, ax = plt.subplots(nrows=2, ncols=6, figsize=(20,40))
    fig.suptitle('Compare classifiers accuracy for various datasets')

    for i, loader in enumerate(data_list):
        plot_bar(cls_list, loader, int(i/6), i%6)

    plt.subplots_adjust(hspace=0.4 ,wspace=0.5)

    plt.show()
