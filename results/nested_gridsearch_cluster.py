# -*- coding: utf-8 -*-


# Authors: Guillaume Gelabert
# License: ?
#
# Nested grid search

import argparse

from sklearn.datasets import load_wine, load_iris, load_digits
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier

from bcamsc.AdversarialCostSensitiveClassification \
                        import CostSensitiveLearning as CSL
from bcamsc.MinimaxProbabilisticTransformation_v1 \
                        import MinimaxProbTransformation as MPT
from bcamsc.MinimaxProbabilisticTransformation_v4 \
                        import Algo as MPT4
from bcamsc.LearningUsingStatisticalInvariants import lusi
from bcamsc.MaximumEntropyMachine import Mem

from bcamsc.datasets import load_mammographic, load_vehicle, load_glass
from bcamsc.datasets import load_haberman, load_column_3C, load_indian_liver
from bcamsc.datasets import load_diabetes


from nested_gridsearch import score_yet_calculated, nested_gridsearch
from nested_gridsearch import scores_save, get_cls_param_grid
from globals import data_list

from time import time

if __name__ == "__main__":

    begin = time()

    parser = argparse.ArgumentParser(description='Create nested gridsearch')

    parser.add_argument('--jobs', required=True, type=int,
                        help='the jobs numbers for GridSearchCv')

    parser.add_argument('--file', required=True, type=str,
                        help='the data to load')

    parser.add_argument('--cls', required=True, type=str,
                        help='The desired classifier. Must be '
                             'chosen between: Mem, Cls, Lusi, Mpt, Svc')

    args = parser.parse_args()

    # The list of the datasets
    """data_list = [load_mammographic, load_vehicle, load_glass,
                 load_haberman, load_column_3C, load_indian_liver,
                 load_diabetes, load_wine, load_iris, load_digits]"""

    # Get the data loader from args
    datas = {l.__name__: l for l in data_list}
    load = datas[args.file]

    # Get the classifier from args
    (cls, pm) = get_cls_param_grid()[args.cls]


    # Run for each dataset and tuple the nested cross-validation
    cls_name = cls.__class__.__name__
    dataset_name = load.__name__

    print('\n{} on {} dataset\n'.format(cls_name, dataset_name))

    if score_yet_calculated(cls_name, load)[0]:
        print('scores calculated yet')
        pass 
    else:
        scores = nested_gridsearch(cls, pm, load, n_jobs=args.jobs,
                                   random_state=42)
        end = time()
        print('{:^10}: {:0.5f} +- {:0.4f}'.format('best_score', 
                                                   scores.mean(),
                                                   scores.std()))
        print('calculation time: {}s'.format(end-begin))
        scores_save(scores, cls_name, load)