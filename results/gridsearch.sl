#!/bin/bash
#SBATCH -J nested_gs   	     # (Placeholder) name of the job
#SBATCH -o gs_%A_%a.out      # output file
#SBATCH -e gs_%A_%a.err      # error file
#SBATCH --time=3-00:00:00    # Maximum run time is 3 days. 
#SBATCH --ntasks=32          # Tasks requested --> c*ntasks requested 
#SBATCH --mem-per-cpu=1G     # Amount of real memory per allocated CPU 
##                             required by the job
#SBATCH -p large             # Partition
##SBATCH --exclusive
##SBATCH --nodelist=n017

## activate the anaconda bcamsc_test virtual environnement
## equivalent to do: conda activate bcamsc_dev4
source $HOME/Guillaume/anaconda3/bin/activate bcamsc_dev4

## output python version to verify that we are using anaconda
python --version

# print the variable defined in batch script gridsearch_all.sh
echo "variable file: "$file
echo "variable cls:  "$cls

python -m nested_gridsearch_cluster --jobs=8 --file=$file --cls=$cls
