# -*- coding: utf-8 -*-


# Authors: Guillaume Gelabert
#
# License: ?
#
# Define global variables for nested grid search

from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.datasets import load_wine, load_iris, load_digits

from bcamsc.datasets import load_mammographic, load_vehicle, load_glass 
from bcamsc.datasets import load_haberman, load_column_3C, load_indian_liver
from bcamsc.datasets import load_diabetes, load_credit, load_adult
from bcamsc.datasets import load_satellite


# Save the scores in the folder ../results/dirname
dirname = 'nested_cv2'

# Seed for random_state
seed = 42

# Define just_plot, a Bool for just ploting the results as the scores will
# comes from cluster computation. If False compute results as indicated in the
# nested_gridsearch module.
just_plot = True

# The list of the datasets used to produce results
data_list = [load_mammographic, load_vehicle, load_glass, load_haberman,
             load_column_3C, load_indian_liver, load_diabetes, load_wine,
             load_digits, load_adult, load_credit, load_satellite] # load_iris

# The inner and outer cross validation used by the function nested_gridsearch
inner_cv = StratifiedShuffleSplit(n_splits=2, train_size=0.7, test_size=0.3,
                                  random_state=seed)

outer_cv = StratifiedShuffleSplit(n_splits=2, train_size=0.7, test_size=0.3,
                                  random_state=seed)
