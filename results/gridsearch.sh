#for loop over the load function

for file in load_iris load_vehicle load_haberman load_diabetes load_wine load_glass load_column_3C load_indian_liver load_mammographic load_digits
do	

	# for loop over the  classificator name

	for cls in RandF Knn Mpt4 Csl Lusi Mpt Mem Svc
	do
		# file and cls are 2 variables, area on the mesmory that are used to
		# store the information needed. Here we create 2 variables, one called
		# "file" that place the load_function name into  memory and one called
 		# "cls" that place the estimator name into memory. Inside the script,
 		# we use "$file" to tell the shell to perform parameter expension and
 		# replace the name of the variable with the variable contents.
 
		echo "train "$cls" classifier on "$file

		# We use the sbatch --export argument to export the variable define 
		# above to the evironnement requiered for the execution of the job

		sbatch --export=file=$file,cls=$cls gridsearch.sl

	done
done