# -*- coding: utf-8 -*-


# Authors: Guillaume Gelabert
# License: ?


import numpy as np
import cvxpy as cvx

from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.utils.validation import check_X_y, check_array, check_is_fitted
from sklearn.utils.multiclass import check_classification_targets
from sklearn.preprocessing import LabelEncoder
from scipy.stats import norm
from .phis import moment_2
from sklearn.utils._random import check_random_state


class Algo(BaseEstimator, ClassifierMixin):
    """Minimax Probabilistic Transformation implemented on the base of Santi
    Pseudo code. For the moment, the inequality part doesn't work as the
    optimization output is None.

    Parameters
    ----------
    phi: function, default moment_2

    equality: Bool, default True

    delta: float, default 0.9
           Only used if equality = False

    deterministic: Bool, default=True
                   If True, the prediction is the argmax of the predict_proba.
                   If False, the prediction is the argmax a random multinomial
                   decision rule.

    random_state : int, RandomState instance or None, optional, default None
        If int, random_state is the seed used by the random number generator;
        If RandomState instance, random_state is the random number generator;
        If None, the random number generator is the RandomState instance used
        by np.random.

    Attributes
    ----------
    alpha_: array-like, shape (m, )
            m is the first dimension of the function phi output

    beta_: array-like, shape (m, )

    gamma_: float

    a_: array-like, shape (m, 1)

    b_: array-like, shape (m, 1)

    m_: int, 
        phi(x,y) --> R^m

    Examples
    --------
    >>> from sklearn.datasets import load_iris
    >>> from sklearn.model_selection import (StratifiedShuffleSplit,
                                             cross_val_score)
    >>> from sklearn.preprocessing import scale
    >>> from bcamsc.MinimaxProbabilitisticTransformation_v2 \
    >>>                                         import algo as Mpt
    >>> random_seed = 42
    >>> clf =  Mpt(random_state=random_seed)
    >>> X, y = load_iris(return_X_y=True)
    >>> X = scale(X)
    >>> cv = StratifiedShuffleSplit(n_splits=10, test_size=0.3,
                    random_state=random_seed)
    >>> res = cross_val_score(clf, X, y, cv=cv, n_jobs=-1)
    >>> print(f"Mpt cv accuracy on iris dataset is: "
              f"{res.mean():.4f} +- {res.std():.4f}")
    Mpt cv accuracy on iris dataset is: 0.9689 +- 0.0204

    """
    def __init__(self, phi=moment_2, equality=True, delta=0.9,
                 deterministic=True, random_state=42):
        self.phi = phi
        self.equality = equality
        self.delta = delta
        self.deterministic = deterministic
        self.random_state = random_state

    def validate_parameters(self):
        '''Validate input parameters.

        '''
        if not isinstance(self.equality, bool):
            raise ValueError("equality must be either True or False")
        if not isinstance(self.deterministic, bool):
            raise ValueError("deterministic must be either True or False")
        # phi to validate.....

    def validate_input(self, X, y):
        '''Validate X and Y

        Parameters
        ----------
        X : array-like, shape (n_samples, m_features)

        y : array-like, shape (n_samples, )
            True labels.

        '''
        X, y = check_X_y(X, y, accept_sparse=False,
                         dtype=np.float64, 
                         estimator='Algo')

        # check that y is of non-regression type
        check_classification_targets(y)

        #labels can be encoded as float, int, or string literals
        self.label_encoder_ = LabelEncoder()
        self.label_encoder_.fit(y)

        # The original class labels
        self.classes_ = self.label_encoder_.classes_

        # The encoded class labels
        self._encoded_classes = self.label_encoder_.transform(self.classes_)

        # Encoded_labels = label_encoder.transform(label_encoder.classes_)
        y = self.label_encoder_.transform(y)

        return X, y

    def _compute_tau(self, X, y):
        """Compute the value of tau(X, y) for the given phi function.
        The mean of [... phi(Xi, yi)...] for i in [0: n_samples]

        Parameters
        ----------
        X : array-like, shape (m_samples, n_features)
            The training input samples.

        y : array-like, shape (m_samples, ).
            The target for X.

        Returns
        -------
        value : array-like, shape (p, 1)
                phi: RmXn --> Rp

        """
        n_classes = self._encoded_classes.size
        tau = self.phi(X[0], y[0, np.newaxis], n_classes)
        for i, (x_, y_) in enumerate(zip(X[1:], y[1:])):
            tau = ((i + 1) * tau
                   + self.phi(x_, y_[np.newaxis], n_classes)) / (i + 2)
        return tau

    def _compute_s(self, X, y):
        """Compute the value of the standard deviation s(X, y) for the given
        phi function. The mean of [... phi(Xi, yi)...] for i in [0: n_samples]

        Parameters
        ----------
        X : array-like, shape (m_samples, n_features)
            The training input samples.

        y : array-like, shape (m_samples, ).
            The target for X.

        Returns
        -------
        value : array-like, shape (p, 1)
                S: RmXn --> Rp

        """
        n_classes = self._encoded_classes.size

        tau = self._compute_tau(X, y)
        
        def std_i(X,y):
            return np.multiply(
                self.phi(X, y, n_classes) - tau,
                self.phi(X, y, n_classes) - tau)

        std = std_i(X[0], y[0:1])
        for i, (x_, y_) in enumerate(zip(X[1:], y[1:, np.newaxis])):
            std = ((i + 1) * std + std_i(x_, y_)) / (i + 2)
        std = np.sqrt(std)
        return std

    def _asymptotically_calibrated(self, X, y):
        """Compute the estimate of alpha, beta, gamma in the case where the 
        statistics a=b.

        Parameters
        ----------
        X: array-like, shape (n_samples, n_features)

        y: array-like, shape (n_samples, )

        Returns
        -------
        alpha: array-like, shape (p, )
                p is the dimension of the function phi output

        beta: array-like, shape (p, )

        gamma: float

        a: array-like, shape (p, 1)

        b: array-like, shape (p, 1)

        """
        a = self._compute_tau(X, y)

        # size of phi(x,y) --> R^m
        p = self.phi(X[0], y[0:1], self._encoded_classes.size).size

        # To estimate
        alpha = cvx.Variable(p)
        gamma = cvx.Variable()
        beta = np.zeros(p)

        cost = a.T @ alpha + gamma
        objective = cvx.Maximize(cost)

        constraints = [cvx.norm1(cvx.pos(
                        self.phi(x, 
                                 self._encoded_classes,
                                 self._encoded_classes.size).T @ alpha
                        + gamma)) <= 1 for x in X]

        prob = cvx.Problem(objective, constraints)
        _ = prob.solve(solver='ECOS', feastol = 1e-20, )

        self.a_ = a
        self.b_ = a
        self.alpha_ = alpha.value
        self.beta_ = beta
        self.gamma_ = gamma.value

    def _approximately_calibrated(self, X, y):
        """Compute the estimate of alpha, beta, gamma in the case where the 
        statistics a!=b.

        Parameters
        ----------
        X: array-like, shape (n_samples, n_features)

        y: array-like, shape (n_samples, )

        """
        n_samples = y.size

        #compute tau and s
        tau = self._compute_tau(X, y)
        s = self._compute_s(X, y)

        #compute z, a and b
        z = norm.ppf(1 - self.delta / (2 * self.m_))
        a = tau - z * s /np.sqrt(n_samples) # (m, 1)
        b = tau + z * s/np.sqrt(n_samples) # (m, 1)

        # fit part
        alpha = cvx.Variable(self.m_)
        gamma = cvx.Variable()
        beta = cvx.Variable(self.m_)

        cost = a.T@alpha - b.T@beta + gamma
        objective = cvx.Maximize(cost)

        constraints = [cvx.norm1(cvx.pos(
                            self.phi(x,
                            self._encoded_classes,
                            self._encoded_classes.size).T
                            @ (alpha - beta)
                            + gamma)) <= 1 for x in X]
        constraints += [alpha >= 0, beta >= 0]

        prob = cvx.Problem(objective, constraints)
        _ = prob.solve('ECOS')
        #_ = prob.solve(solver='SCS', eps=1e-20, max_iters=100000)

        self.a_ = a
        self.b_ = b
        self.alpha_ = alpha.value
        self.beta_ = beta.value
        self.gamma_ = gamma.value

    def _get_upper_bound(self):
        """
        Returns
        -------
        Ru: float
            The upper bound.

        """
        # Check is fit had been called
        check_is_fitted(self, ['a_', 'b_', 'alpha_', 'beta_', 'gamma_'])

        Ru = 1 - self.a_.T @ self.alpha_ + self.b_.T @ self.beta_- self.gamma_
        return Ru[0]

    def _get_lower_bound(self, X):
        """
        Returns
        -------
        Rl: float
            the lower bound
        """
        # Check is fit had been called
        check_is_fitted(self, ['a_', 'b_', 'alpha_', 'beta_', 'gamma_'])

        X = check_array(X)

        # define the variable
        alpha = cvx.Variable(self.m_)
        beta = cvx.Variable(self.m_)
        gamma = cvx.Variable()

        #define objective
        cost = self.a_.T @ alpha - self.b_.T @ beta + gamma
        objective = cvx.Maximize(cost)

        def phi(x):
            return self.phi(x, 
                            self._encoded_classes,
                            self._encoded_classes.size).T

        def t(x):
            first_term = (phi(x) @ (self.alpha_ - self.beta_) 
                          + self.gamma_).clip(min=0.)

            t = (first_term 
                 + (1 - first_term.sum()) / self._encoded_classes.size)
            return t

        constraints = [phi(x) @ (alpha - beta) + gamma <= -t(x) for x in X]

        prob = cvx.Problem(objective, constraints)
        Rl = 1 + prob.solve(solver='ECOS')
        return Rl

    def fit(self, X, y):
        """
        Fit learning using....

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)

        y : array-like, shape (n_samples)

        Returns
        -------
        self : returns an instance of self.

        """
        self.validate_parameters()

        X, y = self.validate_input(X, y)

        if not self.deterministic:
            self._random_state = check_random_state(self.random_state)

        self.m_ = self.phi(X[0], y[0:1], self._encoded_classes.size).size

        if self.equality:
            self._asymptotically_calibrated(X, y)
        else:
            self._approximately_calibrated(X, y)

        return self

    def predict_proba(self, X):
        """Return probability estimates for the test vector X.

        Parameters
        ----------
        X : array-like, shape = (n_samples, n_features)

        Returns
        -------
        proba : array-like, shape = (n_samples, n_classes)
            Returns the probability of the samples for each class in
            the model. The columns correspond to the classes in sorted
            order, as they appear in the attribute ``classes_``.

        """
        # Check is fit had been called
        check_is_fitted(self, ['a_', 'b_', 'alpha_', 'beta_', 'gamma_'])

        X = check_array(X)

        phi = lambda x: self.phi(x,
                                 self._encoded_classes,
                                 self._encoded_classes.size).T

        def t(x):
            t_pos = (phi(x)@(self.alpha_ - self.beta_)
                     + self.gamma_).clip(min=0.)

            # test if proba > 1 and normalize if this is the case

            t_sum = t_pos.sum()

            if t_sum > 1:
                t_pos = t_pos / t_sum

            t = (t_pos + (1 - t_pos.sum()) / self._encoded_classes.size)

            return t

        proba = np.concatenate([t(x)[np.newaxis,:] for x in X], axis=0)
        return proba

    def predict(self, X):
        '''Returns the predicted classes for X samples.

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)

        returns
        -------
        y_pred : array-like, shape (n_samples, )
            y_pred is of the same type as self.classes_.

        '''
        check_is_fitted(self, ['a_', 'b_', 'alpha_', 'beta_', 'gamma_'])
        X = check_array(X)

        if self.deterministic:
            idx = np.argmax(self.predict_proba(X), axis=1)
        else:
            idx = [np.argmax(self._random_state.multinomial(1, q))
                   for q in self.predict_proba(X)]

        return self.classes_[idx]