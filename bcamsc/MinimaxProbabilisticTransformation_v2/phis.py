# -*- coding: utf-8 -*-


# Authors: Santiago Mazuelas
#          Guillaume Gelabert
# License: ?

import numpy as np


def moment_1(X, y, n_classes):
    """Compute the value of the first order moment M1 of X and create a
    one-hot-vector.

    Parameters
    ----------
    X : array-like, shape (n_features, )
        The training input samples.

    y : array-like, shape (m, ).
        The target for X.

    n_classes : int,
        The size of the target space, or number of classes.

    Returns
    -------
    value : array-like, shape 
        ((n_features + 1) * (n_features + 2) * n_classes / 2, m)
        m is the number of targets.

    """
    X = np.concatenate((np.ones((1, )), X)) 
    n = X.size

    value = np.zeros((n_classes, n, y.size))
    for i, j in zip(y, range(y.size)):
        value[i, :, j] = X

    value = value.reshape((n_classes * n, y.size))

    return value

def moment_2(X, y, n_classes):
    """Compute the value of the second order moment M2 of X and create a
    one-hot-vector.

    Parameters
    ----------
    X : array-like, shape (n_features, )
        The training input samples.

    y : array-like, shape (m, ).
        The target for X.

    n_classes : int,
        The size of the target space, or number of classes.

    Returns
    -------
    value : array-like, shape 
        ((n_features + 1) * (n_features + 2) * n_classes / 2, m)

    """
    X = np.concatenate((np.ones((1, )), X)) 
    A = np.dot(X[:, np.newaxis], X[:, np.newaxis].T)
    mask = np.triu_indices(X.size)
    n = len(mask[0])

    value = np.zeros((n_classes, n, y.size))
    for i, j in zip(y, range(y.size)):
        value[i, :, j] = A[mask]

    value = value.reshape((n_classes * n, y.size))

    return value