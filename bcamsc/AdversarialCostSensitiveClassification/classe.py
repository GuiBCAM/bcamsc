# -*- coding: utf-8 -*-


# Authors: Andrea Zanoni
#          Guillaume Gelabert
# License: ?


import warnings

import numpy as np
import cvxpy as cvx

from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.utils import check_random_state, shuffle, gen_batches
from sklearn.utils.validation import check_X_y, check_array, check_is_fitted
from sklearn.utils.multiclass import check_classification_targets
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.base import is_classifier
from sklearn.exceptions import ConvergenceWarning

from ._stochastic_optimizer import SimpleSGDOptimizer


class CostSensitiveLearning(BaseEstimator, ClassifierMixin): 
    """Implementation of the Adversarial Cost-Sensitive Classification 
    describe by algorithm 1 in [1].

    Parameters
    ----------
    confusion_matrix : array-like, shape(n_classes, n_classes), default None
        The confusion matrix cost. When None 
        confusion_matrix = (np.diag(np.ones(len(self.classes_))) * -1 + 1).

    inequality : bool, default False
        p(X,y) in delta(X x y) be a joint distribution of features and labels.
        If U is the set of uncertainty define by the vector of statistics 
        Phi: X x y --> R^L, U is the set of all the distribution consistent
        with the training data. We distinguish 2 cases, depending on how we
        impose the constraints.
        equality:
        U = {p in delta(X x y) | Ep[phi(X,y) = tau}
        inequality:
        U = {p in delta(X x y) | tau - epsilon <=Ep[phi(X,y) <= tau + epsilon}

    epsilon : float, default 0.9
        Only used for the inequality case. See inequality parameter-

    learning_rate_init : double
        The initial learning rate for the ‘constant’, ‘invscaling’ schedules.
        The default value is 1.0.

    max_iter : int, optional, default 200
        Maximum number of iterations for stochastic gradient descent
        The solver iterates until convergence
        (determined by 'tol') or this number of iterations. Note that this
         determines the number of epochs (how many times each data point
        will be used), not the number of gradient steps.

    power_t : double
        The exponent for inverse scaling learning rate [default 0.5].

    tol : float, optional, default 1e-3
        Tolerance for the optimization. When the loss or score is not
        improving by at least ``tol`` for ``n_iter_no_change`` consecutive
        iterations, unless ``lr_schedule`` is set to 'adaptive', convergence
        is considered to be reached and training stops.

    lr_schedule : {'constant', 'invscaling', 'adaptive'}, default 'constant'
        Learning rate schedule for weight updates.

        - 'constant' is a constant learning rate given by
          'learning_rate_init'.

        - 'invscaling' gradually decreases the learning rate at each
          time step 't' using an inverse scaling exponent of 'power_t'.
          effective_learning_rate = learning_rate_init / pow(t, power_t)

        - 'adaptive' keeps the learning rate constant to
          'learning_rate_init' as long as training loss keeps decreasing.
          Each time two consecutive epochs fail to decrease training loss by
          at least tol, or fail to increase validation score by at least tol
          if 'early_stopping' is on, the current learning rate is divided by
          5.

    batch_size : int, optional, default ‘auto’
        Size of minibatches for stochastic optimizer. 
        When set to “auto”, batch_size=min(1, n_samples)

    shuffle : bool, optional, default True
        Whether to shuffle samples in each iteration. Only used when
        solver='stochastic'

    random_state : int, RandomState instance or None, optional, default None
        If int, random_state is the seed used by the random number generator;
        If RandomState instance, random_state is the random number generator;
        If None, the random number generator is the RandomState instance used
        by np.random.

    early_stopping : bool, default False
        Whether to use early stopping to terminate training when validation
        score is not improving. If set to true, it will automatically set
        aside 10% of training data as validation and terminate training when
        validation score is not improving by at least tol for n_iter_no_change
        consecutive epochs.

    validation_fraction : float, optional, default 0.1
        The proportion of training data to set aside as validation set for
        early stopping. Must be between 0 and 1. Only used if early_stopping
        is True

    n_iter_no_change : int, optional, default 10
        Maximum number of epochs to not meet tol improvement.

    deterministic : Bool, default False.
        Whether to use a normal distribution as a frontier of decision during
        the prediction or a deterministic one.

    verbose : bool, optional, default False
        Whether to print progress messages to stdout

    solver : string, default 'ECOS'
        The solver to use in the convex optimization.
        To choose between ['ECOS', 'SCS', 'CVXOPT']


    Attributes
    ----------
    C_ : array_like, shape(n_classes, n_classes)
        The confusion matrix cost.

    theta_ : array-like, shape (m_features, )
        The parameter to estimate.

    classes_ : ndarray, shape(2,)
        The sorted unique values of the labels.

    n_iter_ : int,
        The number of iterations the solver has ran. 
        
    loss_ : float
        The current loss computed with the loss function.

    random_state_ :  RandomState instance
        Randomness is needed after fit, so the random number generator is 
        stored in random_state_ for reproducibility.  

    Examples
    --------
    >>> from sklearn.datasets import load_iris
    >>> from sklearn.preprocessing import scale
    >>> from bcamsc.AdversarialCostSensitiveClassification \
    >>> import CostSensitiveLearning as Csl
    >>> clf = Csl(learning_rate_init=0.1, random_state=42)
    >>> X, y = load_iris(return_X_y=True)
    >>> X = scale(X)
    >>> clf.fit(X, y)
    Iteration 1, loss = 0.27817580
    learning rate: 0.01000
    Iteration 2, loss = 0.16396149
    learning rate: 0.01000

    CostSensitiveLearning(batch_size=1, confusion_matrix=None,
        deterministic=True, early_stopping=False, epsilon=0.9,
        inequality=False, learning_rate_init=0.01, lr_schedule='constant',
        max_iter=2, n_iter_no_change=10, power_t=0.5, random_state=42,
        shuffle=True, solver='ECOS', tol=0.001, validation_fraction=0.1,
        verbose=True)
    >>> print(clf.predict(X, y)
    [1]
    >>> print('{:.10f}'.format(clf.score(X, y)))
    0.8933333333

    Notes
    -----
    [1] : Kaiser, A. Wei, X. Sima, B. and Brian, D.Z.
    "Adversarial Cost-Sensitive Classification". 
    publish in: Proceeding UAI'15 Proceedings of the Thirty-First Conference
    on Uncertainty in Artificial Intelligence, Pages 92-101 
    http://auai.org/uai2015/proceedings/papers/33.pdf

    """
    def __init__(self, confusion_matrix=None, inequality=False,
                 epsilon=0.9, learning_rate_init=1.0, max_iter=10,
                 lr_schedule='constant', power_t=0.5, shuffle=True,
                 random_state=None, batch_size=1, tol=1e-3,
                 early_stopping=False, validation_fraction=0.1,
                 n_iter_no_change=10, deterministic=False, verbose=False,
                 solver='ECOS'):
        
        self.confusion_matrix = confusion_matrix
        self.inequality = inequality
        self.epsilon = epsilon
        self.learning_rate_init = learning_rate_init
        self.max_iter = max_iter
        self.lr_schedule = lr_schedule
        self.power_t = power_t
        self.shuffle = shuffle
        self.random_state = random_state
        self.batch_size = batch_size
        self.tol = tol
        self.early_stopping = early_stopping
        self.validation_fraction = validation_fraction
        self.n_iter_no_change = n_iter_no_change
        self.deterministic = deterministic
        self.verbose = verbose
        self.solver = solver

    def validate_parameters(self):
        '''Validate input parameters.

        '''
        if self.learning_rate_init <= 0:
            raise ValueError("learning_rate_init must be > 0")
        if self.max_iter <= 0:
            raise ValueError("max_iter must be > zero. "
                             "Got {}".format(self.max_iter))

        if self.lr_schedule not in ("constant", "invscaling", "adaptative"):
            raise ValueError("lr_schedule {} is not supported. Must be either"
                             " \"constant\" \"adaptative\"or \"invscaling\"")

        if not isinstance(self.shuffle, bool):
            raise ValueError("shuffle must be either True or False")

        if not isinstance(self.early_stopping, bool):
            raise ValueError("early_stopping must be either True or False,"
                             " got {}".format(self.early_stopping))

        if self.validation_fraction < 0 or self.validation_fraction >= 1:
            raise ValueError("validation_fraction must be >= 0 and < 1, "
                             "got {}".format(self.validation_fraction))

        if self.n_iter_no_change <= 0:
            raise ValueError("n_iter_no_change must be > 0, got %s."
                             % self.n_iter_no_change)

        if not isinstance(self.deterministic, bool):
            raise ValueError("deterministic must be either True or False")

    def validate_input(self, X, y):
        '''Validate X and Y

        Parameters
        ----------
        X : array-like, shape (n_samples, m_features)

        y : array-like, shape (n_samples, )
            True labels.

        '''
        X, y = check_X_y(X, y, accept_sparse=False, warn_on_dtype=True,
                         ensure_min_samples=1, dtype=np.float64,
                         estimator='ACSC')

        # check that y is of non-regression type
        check_classification_targets(y) 

        # labels can be encoded as float, int, or string literals
        self._label_encoder = LabelEncoder()
        self._label_encoder.fit(y)

        # The original class labels
        self.classes_ = self._label_encoder.classes_

        # The encoded class labels
        self._encoded_classes = self._label_encoder.transform(self.classes_)

        # The encoded targets
        y = self._label_encoder.transform(y)

        if self.classes_.size == 1:
            raise ValueError("{0:s} requires 2 classes; got {1:d} class"
                             .format(self.__class__.__name__,
                                     self.classes_.size))

        return X, y

    def _initialize_C(self):
        '''Initialize the confusion matrix automatically if not given.

        '''
        if np.any(self.confusion_matrix) == None:
            self.C_ = (np.diag(np.ones(len(self.classes_))) * -1 + 1)
        else:
            if not isinstance(self.confusion_matrix,
                              (list, tuple, np.ndarray)):
                raise ValueError("confusion matrix must be an instance of "
                                 "(list, tuple, np.ndarray), "
                                 "got {}".format(self.confusion_matrix))
            elif isinstance(self.confusion_matrix, (list, tuple)):
                self.C_ = np.asarray(self.confusion_matrix, dtype=np.float64)
            else:
                self.C_ = self.confusion_matrix.astype(np.float64)

        if not len(self.C_.shape) == 2:
            raise ValueError("confusion matrix must be a matrix, "
                             "got {}".format(self.C_))
        elif not self.C_.shape[0] == self.C_.shape[1]:
            raise ValueError("confusion matrix must be a squared matrix, "
                             "got {}".format(self.C_))

    def _initialize(self, X):
        '''Initialize some attributes for fit methods

        Parameters
        ----------
        X : array-like, shape (n_samples, m_features)

        '''
        self._n_samples, n_features_ = X.shape
        self.n_classes_ = self.classes_.size

        phi_size = ((n_features_ + 2) 
                    * (n_features_ + 1) 
                    * self.n_classes_ / 2)

        # Initialize parameter theta
        theta = np.ones(int(phi_size), dtype=np.float64)
        if self.inequality:
            theta = np.append(theta, theta)
        self.theta_ = theta

        self.random_state_ = check_random_state(self.random_state)

    def _phi(self, X, y):
        """Compute the value of phi(X,y) for the second order moment.

        Parameters
        ----------
        X : array-like, shape (n_features, )
            The training input samples.

        y : array-like, shape (m, ).
            The target for X or self.classes_.

        Returns
        -------
        value : array-like, shape 
            (1*n_features+1)*1*n_features*n_classes/2, m)

        """
        X = np.concatenate((np.ones((1, )), X)) 
        A = np.dot(X[:, np.newaxis], X[:, np.newaxis].T)
        mask = np.triu_indices(X.size)
        n = len(mask[0])

        value = np.zeros((self.n_classes_, n, y.size))
        for i, j in zip(y, range(y.size)):
            value[i, :, j] = A[mask]

        value = value.reshape((self.n_classes_*n, y.size))

        return value

    def _compute_cost_matrix(self, X, y):
        '''Compute the cost matrix given by eq (8) of [1].

        Parameters
        ----------
        X : array-like, shape (n_features, )
            The training input samples.

        y : array-like, shape (1, ).
            The target for X.

        Returns
        -------
        Cp : array-like, shape (n_classes, n_classes)
            The const matrix associated with the given parameters.

        phi_x_l : array-like, shape (1*n_features+1)*1*n_features*n_classes/2,
            m) where m are the number of distinct classes.
            phi_x_l = self._phi(X, self._encoded_classes)
            Returned to avoid computation time in the method _compute_gradient

        phi_x_y : phi_x_l[:, y]
            Returned to avoid computation time in the method _compute_gradient

        '''
        phi_x_l = self._phi(X, self._encoded_classes)

        phi_x_y = phi_x_l[:, y]

        diff = phi_x_l - phi_x_y

        if self.inequality:
            M = self.theta_.size
            theta_left = self.theta_[:M // 2]
            theta_right = self.theta_[M // 2:]

            Cp = (self.C_
                  -np.dot(theta_left, -diff)
                  -np.dot(theta_right, diff))
        else:
            Cp = (self.C_ - np.dot(self.theta_, diff))
        return Cp, phi_x_l, phi_x_y

    def _compute_maximum_entropy_distribution(self, Cp):
        '''Solve linear program given by eq(10) in [1] using cvxpy library.

        parameters
        ----------
        Cp : array-like, shape (n, n)
             The array returns by the function self.compute_cost_matrix().
             Use Cp in the adversary strategy and -Cp.T in the Nash 
             equilibrium strategy.
             Same shape as the confusion matrix C.

        Returns
        -------
        p : array-like, shape (n,)
            The distribution witch give either the adversary strategy or the
            Nash equilibrium strategy if -Cp.T is used.

        v : float.
            Value of the maximum entropy distribution after optimization.

        '''
        n = Cp.shape[0] 

        v = cvx.Variable()
        p = cvx.Variable((n,))
        obj = cvx.Maximize(v)
        constraints = [v <= Cp * p, p >= 0, cvx.sum(p) == 1]
        prob = cvx.Problem(obj, constraints)
        try:
            prob.solve(solver=self.solver)
        except Exception:
            print('SolverExeption')
            prob.solve(self.solver, verbose=True)

        p, v = p.value, v.value

        return p, v

    def _compute_gradient(self, p, phi_x_l, phi_x_y):
        '''Compute the gradient given by algorithm 1 in [1] for one samples
        and one target

        Parameters
        ----------
        p : array-like, shape (n_classes, )
            Parameter of the maximum entropy distribution.

        phi_x_l : array-like, shape (1*n_features+1)*1*n_features*n_classes/2,
            m) where m are the number of distinct classes.
            phi_x_l = self._phi(X, self._encoded_classes)
            Returned by the method _compute_cost_matrix(X, y)

        phi_x_y : phi_x_l[:, y]
            Returned by the method _compute_cost_matrix(X, y)

        Returns
        -------
        g : array-like, shape(m, )
            m depend of the moment order. Here we use the second order moment
            m = (n_features + 1) * (n_features + 2) * n_classes / 2.

        '''
        #phi_x_y = np.ravel(self._phi_x_y)
        phi_x_y = np.ravel(phi_x_y)

        if self.inequality:
            #g_left = np.dot(self._phi_x_l, p) - phi_x_y
            g_left = np.dot(phi_x_l, p) - phi_x_y
            g = np.concatenate((g_left, -g_left))
        else:
            #g = -np.dot(self._phi_x_l, p) + phi_x_y
            g = -np.dot(phi_x_l, p) + phi_x_y

        return g

    def _compute_loss_grad(self, X, y):
        '''Compute the gradient and the loss for X and y

        Parameters
        ----------
        X : array-like, shape (batch_size, n_features)
            The training input samples.

        y : array-like, shape (batch_size, ).
            The target for X.

        Returns
        -------
        loss : float,
            The maximum entropy distribution after cvx optimization
            
        grad : array-like, shape(m, )
            m depend of the moment order. Here we use the second order moment
            m = (n_features +1 ) * (n_features + 2) * n_classes / 2.
            The gradient of the maximum entropy distribution.

        '''
        grad, loss = 0., 0.

        batch_size = len(y)

        for i in range(batch_size):
            x_, y_ = X[i, :], np.array([y[i]])
            Cp, phi_x_l, phi_x_y = self._compute_cost_matrix(x_, y_)
            p, v = self._compute_maximum_entropy_distribution(Cp)
            grad += self._compute_gradient(p, phi_x_l, phi_x_y)
            loss += v 
        grad /= self.batch_size
        loss /= self.batch_size
        return loss, grad

    def _initialize_sgd(self):
        # set attributes for stochastic gradient descent
        self.n_iter_ = 0
        self.t_ = 0
        self.loss_curve_ = []

        self._no_improvement_count = 0
        if self.early_stopping:
            self.validation_scores_ = []
            self.best_validation_score_ = -np.inf
        else:
            self.best_loss_ = np.inf

    def _fit(self, X, y):
        """Stochastic gradient descent implementation. Simplified version of
        the scikit-learn BaseMultilayerPerceptron class.

        Parameters
        ----------
        X : array-like, shape (batch_size, n_features)
            The training input samples.

        y : array-like, shape (batch_size, ).
            The target for X.

        Returns
        -------
        self : object
            Returns self.

        """
        self._initialize_sgd()

        self._optimizer = SimpleSGDOptimizer(
            self.theta_, learning_rate_init=self.learning_rate_init,
            lr_schedule=self.lr_schedule, power_t=self.power_t)

        early_stopping = self.early_stopping
        if early_stopping:
            X, X_val, y, y_val = train_test_split(
                X, y, random_state=self.random_state_,
                test_size=self.validation_fraction)
            if is_classifier(self):
                y_val = (y_val + np.ones(y_val.shape)) * 0.5
                y_val = self._label_encoder.inverse_transform(
                                                         y_val.astype(np.int))

        else:
            X_val = None
            y_val = None

        n_samples = X.shape[0]

        if self.batch_size == 'auto':
            batch_size = min(1, n_samples)
        elif self.batch_size < 1 or self.batch_size > n_samples:
                batch_size = np.clip(self.batch_size, 1, n_samples)
        else:
            batch_size = self.batch_size

        try:
            for _ in range(self.max_iter):
                if self.shuffle:
                    X, y = shuffle(X, y, random_state=self.random_state_)
                accumulated_loss = 0.0

                for batch_slice in gen_batches(n_samples, batch_size):
                    self.theta_ = self._optimizer.param
                    batch_loss, batch_grad = self._compute_loss_grad(
                        X[batch_slice], y[batch_slice])
                    accumulated_loss += batch_loss * (batch_slice.stop -
                                                      batch_slice.start)

                    if self.inequality:
                        batch_grad += (self.epsilon * (batch_slice.stop -
                                                       batch_slice.start)
                                                    / n_samples)

                    # update param
                    self._optimizer.update_param(batch_grad)

                    if self.inequality:
                        self.theta_[self.theta_ < 0] = 0

                self.n_iter_ += 1
                self.loss_ = accumulated_loss / X.shape[0]

                self.t_ += n_samples
                self.loss_curve_.append(self.loss_)
                if self.verbose:
                    print("Iteration %d, loss = %.8f" % (self.n_iter_,
                                                         self.loss_))

                # update no_improvement_count based on training loss or
                # validation score according to early_stopping
                self._update_no_improvement_count(early_stopping, X_val, y_val)

                # for learning rate that needs to be updated at iteration end
                self._optimizer.iteration_ends(self.t_, self.verbose)

                if self._no_improvement_count > self.n_iter_no_change:
                    # not better than last `n_iter_no_change` iterations by 
                    # tol stop or decrease learning rate
                    if early_stopping:
                        msg = ("Validation score did not improve more than "
                                "tol=%f for %d consecutive epochs." % (
                                    self.tol, self.n_iter_no_change))
                    else:
                        msg = ("Training loss did not improve more than "
                                "tol=%f for %d consecutive epochs." % (
                                    self.tol, self.n_iter_no_change))

                    is_stopping = self._optimizer.trigger_stopping(
                        msg, self.verbose)
                    if is_stopping:
                        break
                    else:
                        self._no_improvement_count = 0 

                    if self.n_iter_ == self.max_iter:
                        warnings.warn(
                            "Stochastic Optimizer: Maximum iterations (%d) "
                            "reached and the optimization hasn't converged "
                            "yet."
                            % self.max_iter, ConvergenceWarning)

        except KeyboardInterrupt:
            print("training interrupted by user.")

    def _update_no_improvement_count(self, early_stopping, X_val, y_val):
        if early_stopping:
            self.validation_scores_.append(self.score(X_val, y_val))

            if self.verbose:
                print("Validation score: %f" % self.validation_scores_[-1])
            # update best parameters
            # use validation_scores_, not loss_curve_
            # let's hope no-one overloads .score with mse
            last_valid_score = self.validation_scores_[-1]

            if last_valid_score < (self.best_validation_score_ + 
                                   self.tol):
                self._no_improvement_count += 1
            else:
                self._no_improvement_count = 0

            if last_valid_score > self.best_validation_score_:
                self.best_validation_score_ = last_valid_score
                self._best_theta = self.theta_.copy()

        else:
            if self.loss_curve_[-1] > self.best_loss_ - self.tol:
                self._no_improvement_count += 1
            else:
                self._no_improvement_count = 0
            if self.loss_curve_[-1] < self.best_loss_:
                self.best_loss_ = self.loss_curve_[-1]

    def fit(self, X, y):
        """Fit the model to data matrix X and target(s) y.

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
            The input data.

        y : array-like, shape (n_samples, )
            The class labels.

        Returns
        -------
        self : returns a trained Adversarial Cost-Sensitive Classification
        model.

        """
        X, y = self.validate_input(X, y,)

        self._initialize_C()

        self._initialize(X)

        self.is_fitted_ = True

        self._fit(X, y)

        return self

    def _sample(self, p):
        """Get the estimated encoded label for a given p

        p : array-like, shape (n_features, )
        """
        if self.deterministic:
            idx = np.argmax(p)
            value = self.classes_[idx]
        else:
            u = self.random_state_.uniform(0, 1, 1)
            p_cum = np.cumsum(p)
            value = self.classes_[[i for i, x in enumerate(p_cum)
                                   if x >= u][0]]

        return value

    def predict_proba(self, X):
        """Return probability estimates for the test vector X.

        Parameters
        ----------
        X : array-like, shape = (n_samples, n_features)

        Returns
        -------
        proba : array-like, shape = (n_samples, n_classes)
            Returns the probability of the samples for each class in
            the model. The columns correspond to the classes in sorted
            order, as they appear in the attribute ``classes_``.

        """
        check_is_fitted(self, ['is_fitted_', 'theta_'])
        X = check_array(X, accept_sparse=False, ensure_min_samples=1,
                        warn_on_dtype=True)
        n_samples = X.shape[0]
        # Nash equilibrium strategy
        Q = np.empty((self._encoded_classes.size, n_samples))

        for i in range(n_samples):
            x, y = X[i, :], np.ones(1, dtype=np.int64)
            Cp, _, _ = self._compute_cost_matrix(x, y)
            # The strategy for the Nash equilibrium can be obtained by solving
            # the same linear program with the cost matrix transposed and
            # negated comment found just after eq(10).
            Q[:, i], _ = self._compute_maximum_entropy_distribution(-Cp.T)

        # Cvxpy solve doesn't respect the constraints exactly, so we have to
        # normalize q to have a "real" probability if q_pos > 1.
        Q_pos = Q.clip(min=0.)
        Q_pos_norm1 = Q_pos.sum(axis=0, keepdims=True)

        Q_pos_ = np.where(Q_pos_norm1 < 1., 1.,  Q_pos_norm1)
        Q_normalize = Q_pos / Q_pos_
        Q_normalize_norm1 = Q_normalize.sum(axis=0, keepdims=True)

        proba = (Q_normalize
                 + (1 - Q_normalize_norm1) / self._encoded_classes.size)

        return proba.T # if we want to use cumsum use predict_

    def predict_(self, X):
        """Perform classification on an array of vectors X.

        Parameters
        ----------
        X : array-like, shape = (n_samples, n_features)

        Returns
        -------
        y_pred : array, shape = (n_samples,)
                Predicted target values for X, values are from ``classes_``

        """
        check_is_fitted(self, ['is_fitted_', 'theta_'])
        X = check_array(X, accept_sparse=False, ensure_min_samples=1,
                        warn_on_dtype=True)
        n_samples = X.shape[0]
        y_pred = np.empty(n_samples, dtype=self.classes_.dtype)
        # Nash equilibrium strategy
        Q = np.empty((self._encoded_classes.size, n_samples))

        for i in range(n_samples):
            x, y = X[i, :], np.ones(1, dtype=np.int64)
            Cp, _, _ = self._compute_cost_matrix(x, y)
            # The strategy for the Nash equilibrium can be obtained by solving 
            # the same linear program with the cost matrix transposed and
            # negated comment found just after eq(10).
            Q[:, i], _ = self._compute_maximum_entropy_distribution(-Cp.T)
            y_pred[i] = self._sample(Q[:, i])

        return y_pred

    def predict(self, X):
        '''Returns the predicted classes for X samples.

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)

        returns
        -------
        y_pred : array-like, shape (n_samples, )
            y_pred is of the same type as self.classes_.

        '''
        check_is_fitted(self, ['is_fitted_', 'theta_'])
        X = check_array(X, accept_sparse=False, ensure_min_samples=1,
                        warn_on_dtype=True)

        if self.deterministic:
            idx = np.argmax(self.predict_proba(X), axis=1)
        else:
            idx = [np.argmax(self.random_state_.multinomial(1, q))
                   for q in self.predict_proba(X)]

        return self.classes_[idx]
