import numpy as np

import os

import cvxpy as cvx

from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import ShuffleSplit
from sklearn.base import BaseEstimator
from sklearn.utils.validation import check_X_y, check_is_fitted, check_array
from sklearn.utils.multiclass import check_classification_targets
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import scale
from sklearn.metrics import accuracy_score
from sklearn.base import is_classifier

synthetic_data_dir = ("/home/ggelabert/Documents/MATLAB"
                     + "/MinimaxProbTransformation/classifiers/sample")

x_train_file = 'x_train.csv'
y_train_file = 'y_train.csv'
x_test_file = 'x_test.csv'
y_test_file = 'y_test.csv'

class Phi(BaseEstimator):
    """Compute Phi 
    Parameters
    ----------
    clses: list of n estimator object implementing 'fit'
        The object to use to fit the data. cls = [cls1, ..., clsn]

    """
    def __init__(self, clses, cv):
        self.clses = clses
        self.cv = cv

    def validate_input(self, X, y):
        '''Validate X and Y

        Parameters
        ----------
        X : array-like, shape (n_samples, m_features)

        y : array-like, shape (n_samples, )
            True labels.

        '''
        X, y = check_X_y(X, y, dtype=np.float64, estimator='New')

        # check that y is of non-regression type
        check_classification_targets(y)

        #labels can be encoded as float, int, or string literals
        self.label_encoder_ = LabelEncoder()
        self.label_encoder_.fit(y)

        # The original class labels
        self.classes_ = self.label_encoder_.classes_

        # The encoded class labels
        self._encoded_classes = self.label_encoder_.transform(self.classes_)

        # Encoded_labels = label_encoder.transform(label_encoder.classes_)
        y = self.label_encoder_.transform(y)

        self.n_classes = self.classes_.size

        return X, y

    def fit(self, X, y):
        """
        Fit learning using....

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)

        y : array-like, shape (n_samples)

        Returns
        -------
        self : returns an instance of self.

        """

        for cls in self.clses:
            if not is_classifier(cls):
                raise ValueError("Expected to have classifier object from"
                                 "sklearn. Got %s." % cls)

        for cls in self.clses:
            cls.fit(X, y)

        self.is_fitted_ = True

        return self

    def get_phi(self, X, y):
        """Compute Phi for one samples, and one target. The elemental version
        of Phi."""
        if len(X.shape) == 1:
            X = X[np.newaxis, :]
        if len(y.shape) == 0:
            y = y[np.newaxis]
        if X.shape[0] > 1:
            raise ValueError("X must contain one sample, \
                              or it has shape {}". format(X.shape))
        if y.shape[0] > 1:
            raise ValueError("y must contain one target, \
                              or it has shape {}". format(y.shape))

        X, y = check_X_y(X, y, dtype=np.float64)
        check_is_fitted(self, ['is_fitted_'])
        #y = self.label_encoder_.transform(y)

        # predict samples target for each classifier
        yn = [cls.predict(X) for cls in self.clses]

        n_classifiers = len(self.clses)

        # Obtain phi vector for each sample xi
        v = np.zeros((self.n_classes**(n_classifiers + 1), ))

        idx = y*self.n_classes**n_classifiers
        for i, yi in enumerate(yn):
            idx += yi*self.n_classes**i
        """idx = (y*self.n_classes**3
               + y1*self.n_classes**2
               + y2*self.n_classes**1
               + y3*self.n_classes**0)"""

        idx = idx.astype(np.int)

        v[idx] = 1

        return v

    def get_mean_phi(self, X, y):
        """Compute the mean of all phi(x_, y_) for (x_, y_) in a given
        training set (X, y). The mean is calculated with the prediction part
        of the dataset"""
        X, y = check_X_y(X, y, dtype=np.float64)
        check_is_fitted(self, ['is_fitted_'])
        
        # predict class for each classifier
        y1 = self.cls_1.predict(X)
        y2 = self.cls_2.predict(X)
        y3 = self.cls_3.predict(X)

        # Obtain phi vector for each sample xi
        v = np.zeros((self.n_classes**4, y.size))

        for i, (y_, y_1, y_2, y_3) in \
            enumerate(zip(*(y, y1, y2, y3))):

            idx = (y_*self.n_classes**3
                   + y_1*self.n_classes**2
                   + y_2*self.n_classes**1
                   + y_3*self.n_classes**0)

            v[idx, i] = 1
        mean = np.mean(v, axis=1)
        return mean

    def get_mean_phi_slow(self, X, y):
        """Compute the mean [.....phi(xi, yi)......] for xi in X and yi in y.
        With phi(xi, yi) is a vector of shape (p, )."""

        for i, (x_, y_) in enumerate(zip(*(X, y))):
            if not i:
                a = self.get_phi(x_, y_)
            else:
                a = ((i*a 
                      + self.get_phi(x_, y_))
                     / (i+1))
        return a

    def get_mean_phi_fast(self, X, y):
        "Compute the mean in a fast way"
        X, y = check_X_y(X, y, dtype=np.float64)
        check_is_fitted(self, ['is_fitted_'])

        n_classifiers = len(self.clses)
        n_targets = y.size

        # predict class for each classifier
        idx_pred = [cls.predict(X).astype(np.intp) for cls in self.clses]

        # Create the list of indexes
        idxs = [np.linspace(0, n_targets-1, n_targets, dtype=np.intp),
                y.astype(np.intp)]
        idxs.extend(idx_pred)

        phis = np.zeros((n_targets,) + (self.n_classes,)*(n_classifiers + 1))

        phis[tuple(idxs)] = 1
        mean = phis.mean(axis=0).ravel()
        assert(np.allclose(mean.sum(), 1.))
        return mean

    def get_mean_phi_cv(self, X, y):
        """Compute the cross validated mean."""

        # done the validation outside the fit because we need to know all the
        # class before the cross validation.
        X, y = self.validate_input(X, y)
        print('Encoded classes: {}'.format(self._encoded_classes))
        for i, (train_index, test_index) in enumerate(self.cv.split(X)):
            self.fit(X[train_index], y[train_index])
            if not i:
                a = self.get_mean_phi_fast(X[test_index], y[test_index])
            else:
                a = ((i*a 
                      + self.get_mean_phi_fast(X[test_index], y[test_index])) 
                     / (i+1))

        return a

    def get_phi_xi_classes(self, X, classes):
        """For one sample xi, compute the matrix [phi(xi, 0), ..., phi(xi,j),
        ....phi(xi,n-1)] were [0....n-1] are the n unique target class.

        Parameters
        -----------
        X: array-like, shape (1, n_features)
        classes: array-like, shape (n_classes, )

        Returns
        -------
        MM: array-like, shape (n_classes, p), p is the phi vector size

        """
        MM = np.concatenate([self.get_phi(X, j)[np.newaxis, :]
                             for j in classes[:, np.newaxis]],
                             axis=0)
        return MM

    def get_phi_xi_classes_fast_(self, X):
        """Compute the phi(xi, classes) in a fast way. Basically for every
        sample in X at same time.

        Parameters
        ----------
        X: array-like, shape (n_samples, n_features)
        classes: array-like, shape (n_classes,)
        
        Returns
        -------
        phis: array-like, shape (n_samples, n_classes, p)
        p is the arrival space of phi
        """
        
        X = check_array(X, dtype=np.float64)
        check_is_fitted(self, ['is_fitted_'])
        
        # predict class for each classifier
        y1 = self.cls_1.predict(X)
        y2 = self.cls_2.predict(X)
        y3 = self.cls_3.predict(X)

        n_classes = self.n_classes
        n_samples, _ = X.shape
        classes = self._encoded_classes

        # create the array of indexes
        phi_idx = y1*n_classes**2 + y2*n_classes + y3

        # n_classe*n_samples values
        classes_array = classes[:, np.newaxis]*n_classes**3
        phi_idx = np.repeat(classes_array, n_samples, axis=1) + phi_idx
        broadcast_phi_idx = phi_idx.T.ravel() 

        # create phis, the array of phi(xi, classes) for each xi in X
        phis = np.zeros((n_samples,) + (n_classes,) + (n_classes**4,))

        # create a list [0...n_classes-1,...,0...n_classes-1,..]
        # where there is n_samples times the [0..n_classes] list
        broadcast_classes_idx = np.repeat(classes.reshape(1, n_classes), 
                                      n_samples, axis=0).ravel()

        # create the list of x indexes [0,..0,...,i....i,....,
        # n_samples-1...n_samples-1]
        broadcast_sample_idx = np.repeat(np.arange(n_samples), n_classes)

        phis[broadcast_sample_idx,
             broadcast_classes_idx,
             broadcast_phi_idx] = 1

        return phis

    def get_phi_xi_classes_fast(self, X):
        """Compute the phi(xi, classes) in a fast way. Basically for every
        sample in X at same time.

        Parameters
        ----------
        X: array-like, shape (n_samples, n_features)
        classes: array-like, shape (n_classes,)
        
        Returns
        -------
        phis: array-like, shape (n_samples, n_classes, p)
        p is the arrival space of phi
        """
        
        X = check_array(X, dtype=np.float64)
        check_is_fitted(self, ['is_fitted_'])
        
        # predict target classes for each classifier
        ys = [cls.predict(X).astype(np.intp) for cls in self.clses]

        n_classes = self.n_classes
        n_samples, _ = X.shape
        classes = self._encoded_classes # the classes [0...n_classes-1]
        n_clses = len(self.clses) # the number of classifiers

        # create the array of indexes
        phi_idx = sum(map(lambda x: x[1]*n_classes**(n_clses - (x[0]+1)),
                           enumerate(ys)))

        # n_classe*n_samples values
        classes_array = classes[:, np.newaxis]*n_classes**n_clses
        phi_idx = np.repeat(classes_array, n_samples, axis=1) + phi_idx
        broadcast_phi_idx = phi_idx.T.ravel() 

        # create phis, the array of phi(xi, classes) for each xi in X
        phis = np.zeros((n_samples,) 
                        + (n_classes,) 
                        + (n_classes**(n_clses + 1),))

        # create a list [0...n_classes-1,...,0...n_classes-1,..]
        # where there is n_samples times the [0..n_classes] list
        broadcast_classes_idx = np.repeat(classes.reshape(1, n_classes), 
                                      n_samples, axis=0).ravel()

        # create the list of x indexes [0,..0,...,i....i,....,
        # n_samples-1...n_samples-1]
        broadcast_sample_idx = np.repeat(np.arange(n_samples), n_classes)

        phis[broadcast_sample_idx,
             broadcast_classes_idx,
             broadcast_phi_idx] = 1

        return phis

def get_training_restrictions(n_classes):
    n_c = n_classes
    M = np.zeros((n_c**3, n_c, n_c**4))
    for k1 in range(n_c):
        for k2 in range(n_c):
            for k3 in range(n_c):
                idx = k3*n_c**2 + k2*n_c + k1
                for k4 in range(n_c):
                    M[idx, k4, idx + k4*n_c**3] = 1
    return M

def get_training_restrictions_fast(n_c):
    """Compute the matrix of restrictions for the optimization part in the
    fit method. For the moment just works with 3 classifiers for phi. The
    grain of speed is low compare with get_training_restrictions
    Take care of the size of n_c, if big there could be a memory error. 
    """
    try:
        M = np.zeros((n_c**3, n_c, n_c**4))
    except MemoryError as error:
        msg = f'There is {n_c} classes. The restriction matrix don\'t'\
              f' fit in cpu memory due to the high number of target classes'
        print(msg)
        raise(error)
        #print('There is {} classes, which cause a memory error when creating '
        #      'the restriction matrix'.format(n_c))
    else:
        idx = np.linspace(0, n_c-1, n_c, dtype=np.intp)
        arrays = np.broadcast_arrays(idx[:, np.newaxis, np.newaxis, np.newaxis],
                             idx[:, np.newaxis, np.newaxis],
                             idx[:, np.newaxis],
                             idx)
        idxs = arrays[2]*n_c**2 + arrays[1]*n_c + arrays[0]
        M[idxs, arrays[3], idxs + arrays[3]*n_c**3]=1
        return M

def get_sample_proba(X, phi, alpha, beta, gamma):
    """Compute the probability for one sample.

    Parameter
    ---------
    X: array-like, shape (1, n_features)

    Returns
    -------
    t: array-like, shape(n_classes,)

    """
    if len(X.shape) == 1:
        X = X[np.newaxis, :]
    else:
        pass

    MM = phi.get_phi_xi_classes(X, phi._encoded_classes)
    t = np.clip(MM@(alpha - beta) + gamma, 0., None)
    S = t.sum()
    if S > 1:
        t = t/S
        S = 1.
    const = (1 - S)/phi.n_classes
    t = t + const
    return t

def get_proba_slow(X, phi, alpha, beta, gamma):
    proba = np.concatenate([get_sample_proba(x, phi, alpha,  beta, 
                                            gamma)[np.newaxis,:]
                            for x in X], axis=0)
    return proba

def get_proba_fast(X, phi, alpha, beta, gamma):
    """Return probability estimates for the test vector X.

    Parameters
    ----------
    X : array-like, shape = (n_samples, n_features)

    Returns
    -------
    proba : array-like, shape = (n_samples, n_classes)
        Returns the probability of the samples for each class in
        the model. The columns correspond to the classes in sorted
        order, as they appear in the attribute ``classes_``.

        """
    MM = phi.get_phi_xi_classes_fast(X)
    t = np.clip(MM@(alpha - beta) + gamma, 0., None)
    S = t.sum(axis=1)
    S_clip_min_1 = np.clip(S, 1., None, None)
    S_clip_max_1 = np.clip(S, None, 1., None)
    t = t / S_clip_min_1[:, np.newaxis]
    const = (1.-S_clip_max_1) / phi.n_classes
    t += const[:, np.newaxis]

    return t

def predict(X, phi, alpha, beta, gamma, deterministic):
    proba = get_proba_fast(X, phi, alpha, beta, gamma)
    if deterministic:
        idx = np.argmax(proba, axis=1)
    else:
        idx = [np.argmax(np.random.multinomial(1, q)) for q in proba]
    return phi.classes_[idx]

def fit(phi, X, y):
    # Cross validation for obtaining tau (aux in the new santi code)
    a = phi.get_mean_phi_cv(X, y)
    print('sum(a): {:.5f}'.format(a.sum()))
    assert(np.allclose(sum(a), 1.))

    # restrictiones
    n_c = phi.n_classes
    M = get_training_restrictions(n_c)

    # Optimisation --> alpha, gamma
    p = n_c**4
    alpha = cvx.Variable(p)
    gamma = cvx.Variable()

    cost = a.T@alpha + gamma
    objective = cvx.Maximize(cost)

    constraints = [cvx.sum(cvx.pos(M[i, :, :]@alpha + gamma)) <= 1
                   for i in range(n_c**3)]

    prob = cvx.Problem(objective, constraints)
    _ = prob.solve('GLPK')

    alpha = alpha.value
    beta = np.zeros(p)
    gamma = gamma.value
    b = np.zeros_like(a)

    return a, b, alpha, beta, gamma

def get_sythetic_data():

    with open(os.path.join(synthetic_data_dir, x_train_file)) as f1, \
         open(os.path.join(synthetic_data_dir, y_train_file)) as f2, \
         open(os.path.join(synthetic_data_dir, x_test_file)) as f3, \
         open(os.path.join(synthetic_data_dir, y_test_file)) as f4:

        X_train_ = np.loadtxt(f1, delimiter=',').T
        y_train = np.loadtxt(f2, delimiter=',').T
        X_train = scale(X_train_)

        X_test_ = np.loadtxt(f3, delimiter=',').T
        y_test = np.loadtxt(f4, delimiter=',').T
        X_test = scale(X_test_)

    return (X_train, y_train), (X_test, y_test)

if __name__ == '__main__':

    from time import time

    begin = time()

    print('========= Test MPT version 3 on synthetic datas =========')

    (X_train, y_train), (X_test, y_test) = get_sythetic_data()

    seed = 0
    rep = 100 # number of split in cross validation
    deterministic = True

    cls1 = KNeighborsClassifier(n_neighbors=1, algorithm='kd_tree')
    cls2 = KNeighborsClassifier(n_neighbors=3, algorithm='kd_tree')
    cls3 = KNeighborsClassifier(n_neighbors=5, algorithm='kd_tree')

    rs = ShuffleSplit(n_splits=rep,
                        test_size=.5,
                        random_state=seed)

    phi = Phi([cls1, cls2, cls3], rs)

    a, b, alpha, beta, gamma = fit(phi, X_train, y_train)


    y_pred = predict(X_test, phi, alpha, beta, gamma, deterministic)
    error = 1 - accuracy_score(y_test, y_pred)
    print('error: {:.5f}'.format(error))
    print(f'Calculus finished in {time() - begin}s')
