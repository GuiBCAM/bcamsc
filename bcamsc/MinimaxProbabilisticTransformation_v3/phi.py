import numpy as np

import os

from sklearn.utils.validation import check_X_y, check_is_fitted, check_array
from sklearn.utils.multiclass import check_classification_targets
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import scale
from sklearn.base import is_classifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import ShuffleSplit


cls1 = KNeighborsClassifier(n_neighbors=1, algorithm='kd_tree')
cls2 = KNeighborsClassifier(n_neighbors=3, algorithm='kd_tree')
cls3 = KNeighborsClassifier(n_neighbors=5, algorithm='kd_tree')

rs = ShuffleSplit(n_splits=100, test_size=.5, random_state=42)


class Phi(object):
    """Create a Phi object that implement the two methods required by the
    Algo classifier: get_mean(X, y) and get_phi_classes(X)

    Parameters
    ----------
    clses: List of 3 scikit-learn estimator objects implementing 'fit' and
        'predict' methods. cls = [cls1, cls2, cls3]

    cv: CV splitter:
        A non-estimator family of classes used to split a dataset into a
        sequence of train and test portions (see Cross-validation: evaluating
        estimator performance: https://scikit-learn.org/stable/modules/
        cross_validation.html#cross-validation), by providing split and
        get_n_splits methods. Note that unlike estimators, these do not have
        fit methods and do not provide set_params or get_params. Parameter
        validation may be performed in __init__.

    """
    def __init__(self, clses=[cls1, cls2, cls3], cv=rs):
        self.clses = clses
        self.cv = cv

    def validate_input(self, X, y):
        '''Validate X and Y

        Parameters
        ----------
        X : array-like, shape (n_samples, m_features)

        y : array-like, shape (n_samples, )
            True labels.

        '''
        X, y = check_X_y(X, y, dtype=np.float64)

        # check that y is of non-regression type
        check_classification_targets(y)

        #labels can be encoded as float, int, or string literals
        self.label_encoder_ = LabelEncoder()
        self.label_encoder_.fit(y)

        # The original class labels
        self.classes_ = self.label_encoder_.classes_

        # The encoded class labels
        self._encoded_classes = self.label_encoder_.transform(self.classes_)

        # Encoded_labels = label_encoder.transform(label_encoder.classes_)
        y = self.label_encoder_.transform(y)

        self.n_classes = self.classes_.size

        return X, y

    def fit(self, X, y):
        """Fit the classifiers on X, y.

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)

        y : array-like, shape (n_samples)

        Returns
        -------
        self : returns an instance of self.

        """
        for cls in self.clses:
            if not is_classifier(cls):
                raise ValueError("Expected to have classifier object from"
                                 "sklearn. Got %s." % cls)

        for cls in self.clses:
            cls.fit(X, y)

        self.is_fitted_ = True

        return self

    def get_phi(self, X, y):
        """Compute phi(xi, yi). The elemental version of phi(x, y).
        We don't used it in this optimize code of Phi.

        """
        if len(X.shape) == 1:
            X = X[np.newaxis, :]
        if len(y.shape) == 0:
            y = y[np.newaxis]
        if X.shape[0] > 1:
            raise ValueError("X must contain one sample, \
                              or it has shape {}". format(X.shape))
        if y.shape[0] > 1:
            raise ValueError("y must contain one target, \
                              or it has shape {}". format(y.shape))

        X, y = check_X_y(X, y, dtype=np.float64)
        check_is_fitted(self, ['is_fitted_'])
        #y = self.label_encoder_.transform(y)

        # predict samples target for each classifier
        yn = [cls.predict(X) for cls in self.clses]

        n_classifiers = len(self.clses)

        # Obtain phi vector for each sample xi
        v = np.zeros((self.n_classes**(n_classifiers + 1), ))

        idx = y*self.n_classes**n_classifiers
        for i, yi in enumerate(yn):
            idx += yi*self.n_classes**i

        idx = idx.astype(np.intp)

        v[idx] = 1

        return v

    def _get_mean(self, X, y):
        """Compute the mean of [... phi(Xi, yi)...] for i in [0: n_samples] in
        a fast way for a given set of X samples and Y targets.

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)

        y : array-like, shape (n_samples,)

        Returns
        -------
        mean: array-like, shape (p, 1)
            p is the arrival space size of phi function. phi: RmXn --> Rp. 

        """
        X, y = check_X_y(X, y, dtype=np.float64)
        check_is_fitted(self, ['is_fitted_'])

        n_classifiers = len(self.clses)
        n_targets = y.size

        # predict class for each classifier
        idx_pred = [cls.predict(X).astype(np.intp) for cls in self.clses]

        # Create the list of indexes
        idxs = [np.linspace(0, n_targets-1, n_targets, dtype=np.intp),
                y.astype(np.intp)]
        idxs.extend(idx_pred)

        phis = np.zeros((n_targets,) + (self.n_classes,)*(n_classifiers + 1))

        phis[tuple(idxs)] = 1
        mean = phis.mean(axis=0).ravel()

        assert(np.allclose(mean.sum(), 1.))

        return mean

    def get_mean(self, X, y):
        """Compute the cross validated mean, the final mean that will use in
        the Mpt classifier.

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)

        y : array-like, shape (n_samples)

        Returns
        -------
        mean: array-like, shape (p, 1)
            p is the arrival space size of phi function. phi: RmXn --> Rp. 

        """
        # We perform the validation because we need to know 
        # all the classes before the cross validation.
        X, y = self.validate_input(X, y)

        for i, (train_index, test_index) in enumerate(self.cv.split(X)):

            self.fit(X[train_index], y[train_index])

            if not i:
                mean = self._get_mean(X[test_index], y[test_index])
            else:
                mean = ((i*mean
                      + self._get_mean(X[test_index], y[test_index]))
                     / (i+1))

        # refit estimators on X, y
        self.fit(X, y)
        self.refit_ = True

        return mean

    def get_phi_classes(self, X):
        """Compute the phi(xi, classes) in a fast way for every
        sample in X at same time.

        Parameters
        ----------
        X: array-like, shape (n_samples, n_features)
        classes: array-like, shape (n_classes,)

        Returns
        -------
        phis: array-like, shape (n_samples, n_classes, p)
        p is the arrival space size of phi function.
        """

        X = check_array(X, dtype=np.float64)
        check_is_fitted(self, ['is_fitted_'])
        
        # predict target classes for each classifier
        ys = [cls.predict(X).astype(np.intp) for cls in self.clses]

        n_classes = self.n_classes
        n_samples, _ = X.shape
        classes = self._encoded_classes # the classes [0...n_classes-1]
        n_classifiers = len(self.clses) # the number of classifiers

        # create the array of indexes
        phi_idx = sum(map(lambda x: x[1]*n_classes**(n_classifiers - (x[0]+1)),
                           enumerate(ys)))

        # n_classe*n_samples values
        classes_array = classes[:, np.newaxis]*n_classes**n_classifiers
        phi_idx = np.repeat(classes_array, n_samples, axis=1) + phi_idx
        broadcast_phi_idx = phi_idx.T.ravel()

        # create phis, the array of phi(xi, classes) for each xi in X
        phis = np.zeros((n_samples,) 
                        + (n_classes,) 
                        + (n_classes**(n_classifiers + 1),))

        # create a list [0...n_classes-1,...,0...n_classes-1,..]
        # where there is n_samples times the [0..n_classes] list
        broadcast_classes_idx = np.repeat(classes.reshape(1, n_classes), 
                                      n_samples, axis=0).ravel()

        # create the list of x indexes [0,..0,...,i....i,....,
        # n_samples-1...n_samples-1]
        broadcast_sample_idx = np.repeat(np.arange(n_samples), n_classes)

        phis[broadcast_sample_idx,
             broadcast_classes_idx,
             broadcast_phi_idx] = 1

        return phis