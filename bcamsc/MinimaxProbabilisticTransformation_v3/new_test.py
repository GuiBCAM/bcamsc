import numpy as np

from sklearn.metrics import accuracy_score
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import ShuffleSplit
from sklearn.datasets import load_iris, load_wine
from bcamsc.bcamsc.datasets import *

from new import Phi, fit, predict, get_sythetic_data

if __name__ == '__main__':

    # (X_train, y_train), (X_test, y_test) = get_sythetic_data()

    X, y = load_glass(return_X_y=True)
    rs1 = ShuffleSplit(n_splits=1,
                        test_size=.5,
                        random_state=0)

    [(idx_train, idx_test)] = list(rs1.split(X))

    X_train, y_train = X[idx_train], y[idx_train]
    X_test, y_test = X[idx_test], y[idx_test]

    seed = 0
    rep = 100 # number of split in cross validation
    deterministic = True

    cls1 = KNeighborsClassifier(n_jobs=-1)
    cls2 = SVC(gamma='scale')
    cls3 = RandomForestClassifier(random_state=seed, n_jobs=-1)

    rs = ShuffleSplit(n_splits=rep,
                        test_size=.5,
                        random_state=seed)

    phi = Phi([cls1, cls2, cls3], rs)
    a, b, alpha, beta, gamma = fit(phi, X_train, y_train)

    error_cls = np.empty((3,))
    for i, cls in enumerate([cls1, cls2, cls3]):
        y_train_ = phi.label_encoder_.transform(y_train)
        y_test_ = phi.label_encoder_.transform(y_test)
        error_cls[i] = 1 - cls.fit(X_train,
                                   y_train_).score(X_test, y_test_)
        print('{}: {:.5f}'.format(cls.__class__.__name__, error_cls[i]))

    y_pred = predict(X_test, phi, alpha, beta, gamma, deterministic)
    error = 1 - accuracy_score(y_test, y_pred)
    print('Santi: {:.5f}'.format(error))

