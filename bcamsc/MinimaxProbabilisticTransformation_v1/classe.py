# -*- coding: utf-8 -*-


# Authors: Andrea Zanoni
#          Guillaume Gelabert
# License: ?


import numpy as np
import cvxpy as cvx

from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.utils  import check_random_state
from sklearn.utils.validation import check_X_y, check_array, check_is_fitted
from sklearn.utils.multiclass import check_classification_targets
from sklearn.preprocessing import LabelEncoder


class MinimaxProbTransformation(BaseEstimator, ClassifierMixin):
    """This version of the Minimax Probabilistic Transformation algorithm was
    implemented following the implementation of Andrea Zanoni during his
    internship at BCAM.

    Parameters
    ----------

    random_state : int, RandomState instance or None, optional, default None
        If int, random_state is the seed used by the random number generator;
        If RandomState instance, random_state is the random number generator;
        If None, the random number generator is the RandomState instance used
        by np.random.

    deterministic : Bool, default False.
        Whether to use a normal distribution as a frontier of decision during
        the prediction or a deterministic one.

    Attributes
    ----------
    theta_ : array-like, shape (m_features, )
        The parameter to estimate.

    classes_ : ndarray, shape(2,)
        The sorted unique values of the labels.

    n_iter_ : int,
        The number of iterations the solver has ran. 

    loss_ : float
        The current loss computed with the loss function.

    random_state_ : RandomState instance
        Randomness is needed after fit, so the random number generator is 
        stored in random_state_ for reproducibility.

    Examples
    --------
    >>> from sklearn.datasets import load_iris
    >>> from sklearn.model_selection import (StratifiedShuffleSplit,
                                             cross_val_score)
    >>> from sklearn.preprocessing import scale
    >>> from bcamsc.MinimaxProbabilisticTransformation_v1 \
    >>>                                  import MinimaxProbTransformation as Mpt
    >>> random_seed = 42
    >>> clf =  Mpt(random_state=random_seed)
    >>> X, y = load_iris(return_X_y=True)
    >>> X = scale(X)
    >>> cv = StratifiedShuffleSplit(n_splits=10, test_size=0.3,
                    random_state=random_seed)
    >>> res = cross_val_score(clf, X, y, cv=cv, n_jobs=-1)
    >>> print(f"Mpt cv accuracy on iris dataset is: "
              f"{res.mean():.4f} +- {res.std():.4f}")
    Mpt cv accuracy on iris dataset is: 0.9222 +- 0.0228

    """
    def __init__(self, deterministic=False, random_state=None):

        self.deterministic = deterministic
        self.random_state = random_state

    def validate_parameters(self):
        '''Validate input parameters.

        '''
        if not isinstance(self.deterministic, bool):
            raise ValueError("deterministic must be either True or False")

    def validate_input(self, X, y):
        '''Validate X and Y

        Parameters
        ----------
        X : array-like, shape (n_samples, m_features)

        y : array-like, shape (n_samples, )
            True labels.

        '''
        X, y = check_X_y(X, y, accept_sparse=False,
                         dtype=np.float64, 
                         estimator='MinimaxProbTransformation')

        # check that y is of non-regression type
        check_classification_targets(y)

        #labels can be encoded as float, int, or string literals
        self.label_encoder_ = LabelEncoder()
        self.label_encoder_.fit(y)

        # The original class labels
        self.classes_ = self.label_encoder_.classes_

        # The encoded class labels
        self._encoded_classes = self.label_encoder_.transform(self.classes_)

        # Encoded_labels = label_encoder.transform(label_encoder.classes_)
        y = self.label_encoder_.transform(y)

        return X, y

    def _phi(self, X, y):
        """Compute the value of phi(X,y) for the second order moment.

        Parameters
        ----------
        X : array-like, shape (n_features, )
            The training input samples.

        y : array-like, shape (m, ).
            The target for X or self.classes_.

        Returns
        -------
        value : array-like, shape 
            ((n_features + 1) * (n_features + ") * n_classes / 2, m)

        """
        X = np.concatenate((np.ones((1, )), X)) 
        A = np.dot(X[:, np.newaxis], X[:, np.newaxis].T)
        mask = np.triu_indices(X.size)
        n = len(mask[0])

        n_classes = self._encoded_classes.size
        value = np.zeros((n_classes, n, y.size))
        for i, j in zip(y, range(y.size)):
            value[i, :, j] = A[mask]

        value = value.reshape((n_classes * n, y.size))

        return value

    def _compute_tau(self, X, y):
        """Compute the value of tau(X,y) for the second order moment.
        The mean of [... phi(Xi, yi)...] for i in  [0: n_samples]

        Parameters
        ----------
        X : array-like, shape (m_samples, n_features)
            The training input samples.

        y : array-like, shape (m_samples, ).
            The target for X.

        Returns
        -------
        value : array-like, shape 
            (1 * n_features+1) * 1 * n_features * n_classes/2, m)

        """
        n_samples = X.shape[0]
        tau = self._phi(X[0], y[0, np.newaxis])

        for (x_, y_) in zip(X[1:], y[1:]):
            tau += self._phi(x_, y_[np.newaxis])
        tau /= n_samples

        return tau

    def _fit(self, X, y):

        L = self._phi(X[0], y[0, np.newaxis]).shape[0]

        theta0 = cvx.Variable()
        tau = self._compute_tau(X, y)


        theta = cvx.Variable(L)
        obj = cvx.Maximize(- theta0 - theta.T*tau)
        constr = [cvx.sum(cvx.pos(- theta0 
                                  - theta.T 
                                  * self._phi(X[i], self._encoded_classes)))
                  <= 1 for i in range(X.shape[0])]

        prob = cvx.Problem(obj, constr)

        try:
            _ = prob.solve('ECOS')
        except KeyboardInterrupt:
            print("Training interrupted by user.")

        theta = np.concatenate(([theta0.value], theta.value))

        self.theta_ = theta

    def fit(self, X, y):

        self.validate_parameters()
        X, y = self.validate_input(X, y)

        self.random_state_ = check_random_state(self.random_state)

        self._fit(X, y)

        return self

    def _compute_robust_act_water_filling(self, X):
        """Solve the so called water-filling algorithm for a given X.

        Parameters
        ----------
        X : array-like, shape (n_features, )
            The training input samples.

        Returns
        -------
        q : array-like, shape (n_classes, )
            The conditional probability p(y/X).

        """
        tolerance = 1.e-10
        availability = 1
        # The number of container is equal to the number of classes
        n_classes = self.classes_.size
        q = np.zeros(n_classes)

        theta_init = np.array([self.theta_[1: ]])

        # w is The initial quantity of water in each container
        w = np.squeeze(self.theta_[0] 
                       + np.dot(theta_init, 
                                self._phi(X, self._encoded_classes)))

        # Water filling algorithm:
        while availability > 0 and np.sum(w >= 0) != n_classes:
            w_min = np.min(w)
            indices = (w >= w_min - tolerance) * (w <= w_min + tolerance)

            if np.sum(indices) == n_classes:
                quantity = availability / n_classes
            else:
                w_almost_min = np.min(w[w >= w_min + tolerance])
                quantity = min(w_almost_min - w_min, - w_min,
                               availability / np.sum(indices))

            q[indices] += quantity
            w[indices] += quantity
            availability -= quantity * np.sum(indices)

        if availability > 0:
            q += availability / n_classes

        return q

    def _predict_proba(self, X):
        """Compute the conditional probability q = p(y/x).
        We don't used the method name predict_proba because the output doesn't
        verify the sklearn Api where predict_proba output must verify:
        assert_array_equal(np.argmax(q, axis=1), y_pred).

        Parameters
        ----------
        X : array-like, shape = (n_samples, n_features)

        Returns
        -------
        q : array-like, shape = (n_samples, n_classes)
            The conditional probability of the classes y knowing X. 

        """
        check_is_fitted(self, ["theta_"])
        X = check_array(X, accept_sparse=False)

        q = [self._compute_robust_act_water_filling(x) for x in X]
        q = np.asarray(q, np.float64)

        return q

    def predict(self, X):
        """Perform classification on an array of vectors X.

        Parameters
        ----------
        X : array-like, shape = (n_samples, n_features)

        Returns
        -------
        y_pred : array, shape = (n_samples,)
                Predicted target values for X, values are from ``classes_``

        """
        check_is_fitted(self, ["theta_"])
        X = check_array(X, accept_sparse=False)

        n_samples, _ = X.shape

        q = self._predict_proba(X)
        cum_q = np.cumsum(q, axis=1)

        if self.deterministic:
            idx = np.argmax(q, axis=1)
        else:
            u = self.random_state_.uniform(0, 1, n_samples)
            u = np.asarray(u, np.float64)[:, np.newaxis]
            where = np.where(cum_q >= u, cum_q, 2)
            idx = np.argmin(where, axis=1)

        y_pred = self.classes_[idx]

        return y_pred
