# -*- coding: utf-8 -*-


# Authors: Guillaume Gelabert
# License: ?


import numpy as np
import cvxpy as cvx

from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.utils.validation import check_X_y, check_array, check_is_fitted
from sklearn.utils.multiclass import check_classification_targets
from sklearn.preprocessing import LabelEncoder
from sklearn.utils._random import check_random_state
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import ShuffleSplit

from bcamsc.bcamsc.MinimaxProbabilisticTransformation_v4.phi import Phi
from bcamsc.bcamsc.datasets.load import load_synthetic


class Algo(BaseEstimator, ClassifierMixin):
    """Implementation of the Minimax Probabilistic Transformation for the phi
    function that works with classifiers.he tree classifiers used in this
    implementation are SVM, RandomForest and KNN. 
    For the moment just the equality case is implemented with all the
    optimization constraints compute at the same time. 

    Parameters
    ----------
    svc_C : float, optional (default=1.0)
        Penalty parameter C of the error term for SVM classifier.

    knn_n_neighbors : int, optional (default = 5)
        Number of neighbors to use by default for :meth:`kneighbors` queries.

    rf_n_estimators : int, optional (default=100)
        The number of trees in the forest of the random forest classifer.

    equality: Bool, default True.
        The inequality case is not implemented in this version.

    delta: float, default 0.9
           Only used if equality = False. Not implemented yet.

    deterministic: Bool, default=False
                   If True, the prediction is the argmax of the predict_proba.
                   If False, the prediction is the argmax a random multinomial
                   decision rule.

    random_state : int, RandomState instance or None, optional, default None
        If int, random_state is the seed used by the random number generator;
        If RandomState instance, random_state is the random number generator;
        If None, the random number generator is the RandomState instance used
        by np.random.

    Attributes
    ----------
    phi_ : Instance of the phi class that implement the methods get_mean(X, y)
        and get_phi_classes(X).

    alpha_: array-like, shape (m, )
        m is the first dimension of the function phi output

    beta_: array-like, shape (m, )

    gamma_: float

    a_: array-like, shape (m, 1)

    b_: array-like, shape (m, 1)

    m_: int, 
        m is the first dimension of the function phi output. phi(x,y) --> R^m

    Examples
    --------
    >>> from bcamsc.bcamsc.MinimaxProbabilisticTransformation_v4 import Algo
    >>> from sklearn.datasets import load_iris
    >>> from sklearn.model_selection import (StratifiedShuffleSplit,
                                             cross_val_score)
    >>> from sklearn.model_selection import cross_val_score
    >>> from sklearn.preprocessing import scale
    >>> random_seed = 42
    >>> X, y = load_iris(return_X_y=True)
    >>> X = scale(X)
    >>> cv = StratifiedShuffleSplit(n_splits=10, test_size=0.3,
                                    random_state=random_seed)
    >>> clf =  Algo(equality=True, delta=0.9, deterministic=True,
                    random_state=random_seed)
    >>> res = cross_val_score(clf, X, y, cv=cv, n_jobs=-1)
    >>> print(f"Mpt cv accuracy on iris dataset is: "
              f"{res.mean():.4f} +- {res.std():.4f}")
    Mpt cv accuracy on iris dataset is: 0.9356 +- 0.0252

    """
    def __init__(self, svc_C=1.0, knn_n_neighbors=2, rf_n_estimators=100,
                 equality=True, delta=0.9, deterministic=False,
                 random_state=None):
        self.svc_C = svc_C
        self.knn_n_neighbors = knn_n_neighbors
        self.rf_n_estimators = rf_n_estimators
        self.equality = equality
        self.delta = delta
        self.deterministic = deterministic
        self.random_state = random_state

    def initialize(self):
        """initialize the phi_ attribute.
        Create the three instances of support vector machine,
        k nearest neighbours and random forest classifiers used in the phi
        function as well as the cross validation instance used to calculated
        the mean of phi funcion.

        """
        self._random_state = check_random_state(self.random_state)

        cls1 = SVC(
            kernel="rbf",
            gamma='scale',
            C = self.svc_C,
            random_state=self._random_state
            )

        cls2 = RandomForestClassifier(
            n_estimators=self.rf_n_estimators,
            random_state=self._random_state,
            n_jobs=1)

        cls3 = KNeighborsClassifier(
            n_neighbors=self.knn_n_neighbors,
            algorithm='kd_tree',
            n_jobs=1,
            )

        rs = ShuffleSplit(n_splits=10, test_size=.5,
                           random_state=self._random_state)

        self.phi_ = Phi([cls1, cls2, cls3], rs)

    def validate_parameters(self):
        '''Validate input parameters.

        '''
        if not isinstance(self.equality, bool):
            raise ValueError("equality must be either True or False")
        if not self.equality == True:
            raise ValueError("inequality case not implemented yet. "
                             "Change equality parameter to true.")
        if not isinstance(self.deterministic, bool):
            raise ValueError("deterministic must be either True or False")
        # phi to validate.....

    def validate_input(self, X, y):
        '''Validate X and Y

        Parameters
        ----------
        X : array-like, shape (n_samples, m_features)

        y : array-like, shape (n_samples, )
            True labels.

        '''
        X, y = check_X_y(X, y, accept_sparse=False,
                         dtype=np.float64,
                         ensure_min_samples=2,
                         estimator='MPT_V3')

        # check that y is of non-regression type
        check_classification_targets(y)

        #labels can be encoded as float, int, or string literals
        self.label_encoder_ = LabelEncoder()
        self.label_encoder_.fit(y)

        # The original class labels
        self.classes_ = self.label_encoder_.classes_

        # The encoded class labels
        self._encoded_classes = self.label_encoder_.transform(self.classes_)

        # Encoded_labels = label_encoder.transform(label_encoder.classes_)
        y = self.label_encoder_.transform(y)

        return X, y

    def _get_optimization_constraints(self):
        """Compute the matrix of restrictions for the optimization part 
        in the fit method. 
        For the moment just works with 3 classifiers for phi. The
        grain of speed is low compare with simple loop over dimensions.
        Take care of the number of labels, n_c. 
        If n_c is too big there could be a memory error.

        Returns
        -------
        M: array-like, shape (n_c**3, n_c, n_c**4) with nc is the number of
           classes in the training set.
           M is the matrix of constraints used in the optimization part of the
           methods _asymptotically_calibrated(X, y)
           
        """
        # The number of classes in the training set
        n_c = self.classes_.size

        try:
            M = np.zeros((n_c**3, n_c, n_c**4))

        except MemoryError as error:
            msg = f'There is {n_c} classes. The restriction matrix don\'t'\
                  f' fit in cpu memory due to the high number of target'\
                  f' classes'

            print(msg)
            raise(error)

        else:
            idx = np.linspace(0, n_c-1, n_c, dtype=np.intp)
            arrays = np.broadcast_arrays(
                                idx[:, np.newaxis, np.newaxis, np.newaxis],
                                idx[:, np.newaxis, np.newaxis],
                                idx[:, np.newaxis],
                                idx
                                        )
            idxs = (arrays[2]*n_c**2
                    + arrays[1]*n_c
                    + arrays[0]
                    )

            M[idxs, arrays[3], idxs + arrays[3]*n_c**3] = 1

            return M

    def _asymptotically_calibrated(self, X, y):
        """Compute the estimate of alpha, beta, gamma in the equality case.

        Parameters
        ----------
        X: array-like, shape (n_samples, n_features)

        y: array-like, shape (n_samples, )

        Returns
        -------
        alpha: array-like, shape (p, )
                p is the dimension of the function phi output

        beta: array-like, shape (p, )

        gamma: float

        a: array-like, shape (p, 1)

        b: array-like, shape (p, 1)

        """
        a = self.phi_.get_mean(X, y)
        assert(np.allclose(sum(a), 1.))

        # restrictiones5
        M = self._get_optimization_constraints()

        # Optimisation --> alpha, gamma
        n_c = self.classes_.size

        alpha = cvx.Variable(n_c**4)
        gamma = cvx.Variable()

        cost = a.T@alpha + gamma
        objective = cvx.Maximize(cost)

        constraints = [cvx.sum(cvx.pos(M[i, :, :]@alpha + gamma)) <= 1
                       for i in range(n_c**3)]

        prob = cvx.Problem(objective, constraints)
        _ = prob.solve('GLPK')

        self.a_ = a
        self.b_ = np.zeros_like(a)
        self.alpha_ = alpha.value
        self.beta_ = np.zeros(n_c**4)
        self.gamma_ = gamma.value

    def _get_upper_bound(self):
        """
        Returns
        -------
        Ru: float
            The upper bound.

        """
        # Check is fit had been called
        check_is_fitted(self, ['a_', 'b_', 'alpha_', 'beta_', 'gamma_'])

        Ru = 1 - self.a_.T @ self.alpha_ + self.b_.T @ self.beta_- self.gamma_
        return Ru[0]

    def _get_lower_bound(self, X):
        """
        Returns
        -------
        Rl: float
            the lower bound
        """
        # Check is fit had been called
        check_is_fitted(self, ['a_', 'b_', 'alpha_', 'beta_', 'gamma_'])

        X = check_array(X)

        # define the variable
        print('method _get_lower_bound not implemented yet')
        pass

    def fit(self, X, y):
        """
        Fit learning using....

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)

        y : array-like, shape (n_samples)

        Returns
        -------
        self : returns an instance of self.

        """
        self.validate_parameters()

        X, y = self.validate_input(X, y)

        self.initialize()

        if self.equality:
            self._asymptotically_calibrated(X, y)
        else:
            self.m_ = self.phi_.get_phi(X[0], y[0:1]).size
            self._approximately_calibrated(X, y)

        return self

    def predict_proba(self, X):
        """Return probability estimates for the test vector X.

        Parameters
        ----------
        X : array-like, shape = (n_samples, n_features)

        Returns
        -------
        proba : array-like, shape = (n_samples, n_classes)
            Returns the probability of the samples for each class in
            the model. The columns correspond to the classes in sorted
            order, as they appear in the attribute ``classes_``.

        """
        # Check is fit had been called
        check_is_fitted(self, ['a_', 'b_', 'alpha_', 'beta_', 'gamma_'])

        X = check_array(X)

        MM = self.phi_.get_phi_classes(X)

        proba = MM@(self.alpha_ - self.beta_) + self.gamma_

        proba = np.clip(proba, 0., None)

        S = proba.sum(axis=1)
        S_clip_min_1 = np.clip(S, 1., None, None)
        S_clip_max_1 = np.clip(S, None, 1., None)

        proba = proba / S_clip_min_1[:, np.newaxis]

        const = (1.-S_clip_max_1) / self.phi_.n_classes
        proba += const[:, np.newaxis]

        return proba

    def predict(self, X):
        '''Returns the predicted classes for X samples.

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)

        returns
        -------
        y_pred : array-like, shape (n_samples, )
            y_pred is of the same type as self.classes_.

        '''
        check_is_fitted(self, ['a_', 'b_', 'alpha_', 'beta_', 'gamma_'])
        X = check_array(X)

        proba = self.predict_proba(X)

        if self.deterministic:
            idx = np.argmax(proba, axis=1)
        else:
            idx = [np.argmax(self._random_state.multinomial(1, q))
                   for q in proba]

        return self.classes_[idx]


if __name__ == '__main__':

    print('========= Test MPT version 4 on synthetic data =========')

    from time import time

    begin = time()

    (X_train, y_train), (X_test, y_test) = load_synthetic()

    Mpt = Algo(random_state=0)
    Mpt.fit(X_train, y_train)

    error = 1 - Mpt.score(X_test, y_test)

    print('error: {:.20f}'.format(error))
    assert(np.allclose(error, 0.15100000000000002309))

    print(f'Calculus finished in {time() - begin}s')
