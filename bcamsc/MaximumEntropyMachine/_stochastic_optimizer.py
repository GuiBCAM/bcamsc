"""Stochastic optimization method for Mem.
Adapation of scikit-learn code: https://github.com/scikit-learn/scikit-learn/blob/master/sklearn/neural_network/_stochastic_optimizers.py
"""

class BaseOptimizer:
    """Base (Stochastic) gradient descent optimizer

    Parameters
    ----------
    param : array-like, shape (m_features, )
        The parameter to estimate
    learning_rate_init : float, optional, default 0.1
        The initial learning rate used. It controls the step-size in updating
        the weights

    Attributes
    ----------
    learning_rate : float
        the current learning rate

    """

    def __init__(self, param, learning_rate_init=0.1):
        self.param = param
        self.learning_rate_init = learning_rate_init
        self.learning_rate = float(learning_rate_init)

    def update_param(self, grad):
        """Update parameter with given gradients

        Parameters
        ----------
        grads : list, length = len(params)
            Containing gradients with respect to coefs_ and intercepts_ in MLP
            model. So length should be aligned with params

        """
        update = self._get_update(grad)
        self.param += update

    def iteration_ends(self, time_step):
        """Perform update to learning rate and potentially other states at the
        end of an iteration

        """
        pass

    def trigger_stopping(self, msg, verbose):
        """Decides whether it is time to stop training

        Parameters
        ----------
        msg : str
            Message passed in for verbose output
        verbose : bool
            Print message to stdin if True

        Returns
        -------
        is_stopping : bool
            True if training needs to stop

        """
        if verbose:
            print(msg + " Stopping.")
        return True


class SimpleSGDOptimizer(BaseOptimizer):
    """Stochastic gradient descent optimizer

    Parameters
    ----------
    param : array-like, shape (m_features, )
        The parameter to estimate
    learning_rate_init : float, optional, default 0.1
        The initial learning rate used. It controls the step-size in updating
        the weights
    lr_schedule : {'constant', 'adaptive', 'invscaling'}, default 'constant'
        Learning rate schedule for weight updates.
        -'constant', is a constant learning rate given by
         'learning_rate_init'.
        -'invscaling' gradually decreases the learning rate 'learning_rate_' at
          each time step 't' using an inverse scaling exponent of 'power_t'.
          learning_rate_ = learning_rate_init / pow(t, power_t)
        -'adaptive', keeps the learning rate constant to
         'learning_rate_init' as long as the training keeps decreasing.
         Each time 2 consecutive epochs fail to decrease the training loss by
         tol, or fail to increase validation score by tol if 'early_stopping'
         is on, the current learning rate is divided by 5.

    Attributes
    ----------
    learning_rate : float
        the current learning rate

    """

    def __init__(self, param, learning_rate_init=0.1, lr_schedule='constant',
                 power_t=0.5):
        super().__init__(param, learning_rate_init)

        self.lr_schedule = lr_schedule
        self.power_t = power_t

    def iteration_ends(self, time_step, verbose):
        """Perform updates to learning rate and potential other states at the
        end of an iteration

        Parameters
        ----------
        time_step : int
            number of training samples trained on so far, used to update
            learning rate for 'invscaling'

        """
        if self.lr_schedule == 'invscaling':
            self.learning_rate = (float(self.learning_rate_init) /
                                  (time_step + 1) ** self.power_t)
        if verbose:
            print('learning rate: {:.5f}'.format(self.learning_rate))

    def trigger_stopping(self, msg, verbose):
        if self.lr_schedule != 'adaptive':
            if verbose:
                print(msg + " Stopping.")
            return True

        if self.learning_rate <= 1e-6:
            if verbose:
                print(msg + " Learning rate too small. Stopping.")
            return True

        self.learning_rate /= 5.
        if verbose:
            print(msg + " Setting learning rate to %f" %
                  self.learning_rate)
        return False

    def _get_update(self, grad):
        """Get the values used to update param with given gradients

        Parameters
        ----------
        grad : array_like, shape (m_features, )
            The gradient of the loss with respect to the parameter.

        Returns
        -------
        updates : list, length = len(grads)
            The values to add to params

        """
        update = - self.learning_rate * grad

        return update
