# -*- coding: utf-8 -*-


# Authors: Guillaume Gelabert
# License: ?


import warnings

import cvxpy as cp
import numpy as np

from scipy import argmax, multiply
from scipy import expand_dims, concatenate, dot, mean
from scipy.linalg import norm
from sklearn.base import BaseEstimator, ClassifierMixin, is_classifier
from sklearn.metrics import get_scorer
from sklearn.utils.multiclass import check_classification_targets
from sklearn.utils.validation import check_X_y, check_array, check_is_fitted
from sklearn.utils import gen_batches, check_random_state, shuffle
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.exceptions import ConvergenceWarning
from sklearn.multiclass import OneVsRestClassifier, OneVsOneClassifier

from ._stochastic_optimizer import SimpleSGDOptimizer
from cvxpy.problems.solvers import solver

class _Mem(BaseEstimator):
    """Implementation of the maximun entropy machine for 0-1 loss (4.3.2)
    describe by eq(28) in [1]. Binary classifier.

    Parameters
    ----------
    alpha_init : float, default=0.0
        Initial value of the linear predictor.

    lambd: float, default=2e-5
        L2 penalty (regularization term) parameter in eq(28).

    learning_rate_init : double
        The initial learning rate for the ‘constant’, ‘invscaling’ schedules.
        The default value is 1.0.

    power_t : double
        The exponent for inverse scaling learning rate [default 0.5].

    tol : float, optional, default 1e-3
        Tolerance for the optimization. When the loss or score is not improving
        by at least ``tol`` for ``n_iter_no_change`` consecutive iterations,
        unless ``lr_schedule`` is set to 'adaptive', convergence is
        considered to be reached and training stops.

    max_iter : int, optional, default 200
        Only for stochastic solver. Maximum number of iterations.
        The solver iterates until convergence
        (determined by 'tol') or this number of iterations. Note that this
         determines the number of epochs (how many times each data point
        will be used), not the number of gradient steps.

    scoring : callable or None, optional, default: None
        A string (see model evaluation documentation) or
        a scorer callable object / function with signature
        ``scorer(estimator, X, y)``. For a list of scoring functions
        that can be used, look at :mod:`sklearn.metrics`. The
        default scoring option used is accuracy_score.

    lr_schedule : {'constant', 'invscaling', 'adaptive'}, default 'constant'
        Learning rate schedule for weight updates.

        - 'constant' is a constant learning rate given by
          'learning_rate_init'.

        - 'invscaling' gradually decreases the learning rate at each
          time step 't' using an inverse scaling exponent of 'power_t'.
          effective_learning_rate = learning_rate_init / pow(t, power_t)

        - 'adaptive' keeps the learning rate constant to
          'learning_rate_init' as long as training loss keeps decreasing.
          Each time two consecutive epochs fail to decrease training loss by at
          least tol, or fail to increase validation score by at least tol if
          'early_stopping' is on, the current learning rate is divided by 5.

        Only used when ``solver='stochastic'``.

    batch_size : int, optional, default ‘auto’
        Size of minibatches for stochastic optimizer. 
        When set to “auto”, batch_size=min(1, n_samples)

    shuffle : bool, optional, default True
        Whether to shuffle samples in each iteration. Only used when
        solver='stochastic'

    random_state : int, RandomState instance or None, optional, default None
        If int, random_state is the seed used by the random number generator;
        If RandomState instance, random_state is the random number generator;
        If None, the random number generator is the RandomState instance used
        by np.random.

    solver : string,
        Possible choices are in the list ['sgd', 'cvx']

    verbose : bool, optional, default False
        Whether to print progress messages to stdout

    early_stopping : bool, default False
        Whether to use early stopping to terminate training when validation
        score is not improving. If set to true, it will automatically set
        aside 10% of training data as validation and terminate training when
        validation score is not improving by at least tol for n_iter_no_change
        consecutive epochs. Only effective when solver=’stochastic’.

    validation_fraction : float, optional, default 0.1
        The proportion of training data to set aside as validation set for
        early stopping. Must be between 0 and 1. Only used if early_stopping
        is True

    n_iter_no_change : int, optional, default 10
        Maximum number of epochs to not meet tol improvement.
        Only effective when solver=’stochastic’ .

    Attributes
    ----------
    alpha : array-like, shape (m_features, )
        The parameter to estimate.

    classes_ : ndarray, shape(2,)
        The sorted unique values of the labels.

    Examples
    --------
    >>> import numpy as np
    >>> X = np.array([[-1, -1], [-2, -1], [1, 1], [2, 1]])
    >>> y = np.array([1, 1, 2, 2])
    >>> from bcamsc.MaximumEntropyMachine import Mem
    >>> clf = Mem()
    >>> clf.fit(X, y)
    Mem(alpha_init=0.0, batch_size='auto', early_stopping=False, lambd=2e-05,
      learning_rate_init=1.0, lr_schedule='invscaling', max_iter=10,
      multi_class='one_vs_rest', n_iter_no_change=10, n_jobs=None,
      power_t=0.5, random_state=None, scoring='accuracy', shuffle=True,
      solver='sgd', tol=0.001, validation_fraction=0.1, verbose=False)
    >>> print(clf.predict([[-0.8, -1]]))
    [1]

    Notes
    -----
    [1] : Farzan, F. and David, T.
    "A Minimax Approach to Supervised Learning". arXiv:1606.02206v5 [stat.ML].
    4 Jul 2017

    """
    def __init__(self, alpha_init=0.0, lambd=2e-5, learning_rate_init=1.,
                 power_t=0.5, tol=0.001, max_iter=10,
                 solver='sgd', batch_size='auto', 
                 lr_schedule='invscaling', shuffle=True, random_state=None,
                 scoring='accuracy', verbose=False, early_stopping=False,
                 validation_fraction=0.1, n_iter_no_change=10):
        self.alpha_init = alpha_init
        self.lambd = lambd
        self.learning_rate_init = learning_rate_init
        self.power_t = power_t
        self.tol = tol
        self.max_iter = max_iter
        self.scoring = scoring
        self.batch_size = batch_size
        self.lr_schedule = lr_schedule
        self.shuffle = shuffle
        self.random_state = random_state
        self.solver = solver
        self.verbose = verbose
        self.early_stopping = early_stopping
        self.validation_fraction = validation_fraction
        self.n_iter_no_change = n_iter_no_change

    def _validate_params(self):
        '''Validate input params
        
        '''
        if self.solver not in ['sgd', 'cvx']:
            raise ValueError("The solver {} is not supported. "
                             "Expected one of: {}" .format
                             (self.solver, ", ".join(['sgd', 'cvx'])))
        if self.solver == 'sgd':
            if self.learning_rate_init <= 0 and self.solver == 'sgd':
                raise ValueError("learning_rate_init must be > 0")
            if self.max_iter < 0:
                raise ValueError("max_iter must be >= zero. "
                             "Got {}".format(self.max_iter))
            if not isinstance(self.shuffle, bool):
                raise ValueError("shuffle must be either True or False")
            if self.lr_schedule not in ("constant", "invscaling",
                                        "adaptative"):
                raise ValueError("lr_schedule {} is not supported. Must be "
                                 "either \"constant\" \"adaptative\"or "
                                 "\"invscaling\"")
            if not isinstance(self.early_stopping, bool):
                raise ValueError("early_stopping must be either True or "
                                 "False, got {}".format(self.early_stopping))
            if self.validation_fraction < 0 or self.validation_fraction >= 1:
                raise ValueError("validation_fraction must be >= 0 and < 1, "
                                 "got {}".format(self.validation_fraction))
            if self.n_iter_no_change <= 0:
                raise ValueError("n_iter_no_change must be > 0, got %s."
                             % self.n_iter_no_change)

    def _validate_X_y(self, X, y):
        '''Validate that the target y are binary and not unique

        Parameters
        ----------
        X : array-like, shape (n_samples, m_features)

        y : array-like, shape (n_samples, )
            True labels.

        '''
        X, y = check_X_y(X, y, accept_sparse=False, 
                         dtype=np.float64, estimator='Mem')

        # check that y is of non-regression type
        check_classification_targets(y)

        #labels can be encoded as float, int, or string literals
        self.label_encoder_ = LabelEncoder()
        self.label_encoder_.fit(y)
        # The original class labels
        self.classes_ = self.label_encoder_.classes_
        # Encoded_labels = label_encoder.transform(label_encoder.classes_)
        y = self.label_encoder_.transform(y)
        #The classes must be -1 or 1 for loss function calculus
        y = (y - np.ones(y.shape) * 0.5) * 2.0

        if self.classes_.size > 2:
            raise ValueError("%s supports only binary classification. "
                             "y contains classes %s"
                             % (self.__class__.__name__, self.classes_))
        elif self.classes_.size == 1:
            raise ValueError("{0:s} requires 2 classes; got {1:d} class"
                             .format(self.__class__.__name__,
                                     self.classes_.size))

        return X, y

    def _compute_loss(self, X, y, alpha, lambd):
        """ If eq(28) is min[J(alpha)] we compute J for a given alpha.
        Compute also indices_max, the arg of the max in (28) for later 
        calculation of the derivative.

        Parameters
        ----------
        X : array-like, shape (n_samples, m_features)

        y : array-like, shape (n_samples, )
            True labels.

        alpha : array-like, shape (m_features, )
            Parameters of J.

        lambd : float.
            The regularization coefficient.

        Returns
        -------
        loss: float.
            The value if J for given x,y,alpha and lambd.

        indices_max :array-like, shape (n_samples, )
            The indices that determine which one of the 3 possible functions \
            we have to choose for calculating the sub gradient at each sample.

        """
        n_samples = X.shape[0]
        alphaT_x = dot(X, alpha)
        y_alphaT_x = expand_dims(multiply(alphaT_x, y), axis=1)
        vector_one = np.ones_like(y_alphaT_x)
        # calculate the 3 possible functions that are inside the max.
        # Eq(28) max(f1, f2, f3)
        f1 = np.zeros_like(y_alphaT_x)
        f2 = (vector_one - y_alphaT_x) * 0.5
        f3 = -1.0 * y_alphaT_x
        functions = concatenate([f1, f2, f3], axis=1)
        # Select position of the max for later derivative calculation
        indices_max = argmax(functions, axis=1)
        loss = mean(functions[range(n_samples), indices_max])
        # we can also do loss = mean(np.max(functions, axis=1)) but slower
        regularisation = lambd * norm(alpha, ord=2) ** 2
        loss += regularisation
        return loss, indices_max

    def _compute_gradient(self, X, y, indices_max, lambd, alpha):
        """ Compute the gradient of the loss function without regularization
        part. Compute the 3 possible gradients of the max function for each
        samples and put them in a 3D tensor (m, n, 3). Then we chose the
        rights gradients with the max index and take the mean.

        Parameters
        ----------
        X : array-like, shape (n_samples, m_features)

        y : array-like, shape (n_samples, )
            True labels.

        indices_max :array-like, shape (n_samples, )
            The indices that determine which one of the 3 possible functions
            we have to choose for calculating the sub gradient at each sample.

        lambd : float
            L2 penalty (regularization term) parameter in eq(28).

        alpha : array-like, shape (m_features, )
            The parameter to estimate.

        Returns
        -------
        grad : array-like, shape (m_features, )
            The gradient of the loss function for given parameters.

        """
        n_samples, n_features = X.shape
        all_gradients = np.zeros((n_samples, n_features, 3), dtype=np.float64)
        x_y = X.copy()
        x_y *= y[:, np.newaxis]
        all_gradients[:, :, 1] = -0.5 * x_y
        all_gradients[:, :, 2] = -1.0 * x_y

        gradients = all_gradients[range(n_samples), :, indices_max]
        gradient = mean(gradients, axis=0)

        gradient += 2 * lambd * alpha

        # to avoid overflow
        gradient = gradient.clip(-1e12, 1e12)

        return gradient

    def _compute_loss_grad(self, X, y):
        """Compute the loss function and its corresponding derivative with 
        respect to parameter alpha.

        Parameters
        ----------
        X : {array-like, sparse matrix}, shape (n_samples, n_features)
            The input data.
        y : array-like, shape (n_samples,)
            The target values.

        Returns
        -------
        loss : float
        grad : array_like, shape (m_features, )

        """
        loss, max_indexes = self._compute_loss(X, y, self.alpha, self.lambd)
        grad = self._compute_gradient(X, y, max_indexes, self.lambd, self.alpha)
        return loss, grad

    def _get_minimax_cvx(self, X, y, lambd):
        '''Create the cvx problem for later optimisation:

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
            The training input samples.

        y : array-like, shape (n_samples, ).
            Binary target for X

        lambd: float, default=0.00002
        L2 penalty (regularization term) parameter in eq(28).

        Return
        ------
        prob: instance of a cvx probleme class

        alpha : array-like, shape (m_features, )
            The parameter to estimate.

        '''
        # declare the variables and parameters for cvx problem definition
        alpha = cp.Variable(shape=(X.shape[1]), name='alpha')
        lambd = cp.Parameter(name='lambda', nonneg=True, value=lambd)

        # Create the problem to optimize
        f1 = np.zeros_like(y)
        f2 = cp.multiply(-y, cp.matmul(X, alpha))
        f3 = (1. + f2) / 2

        maxi = cp.maximum(f1, f2, f3)
        n = np.float64(len(y)) # samples numbers
        loss = cp.sum(maxi)
        reg = cp.sum_squares(alpha)
        prob = cp.Problem(cp.Minimize(loss / n + lambd * reg))

        return prob, alpha

    def _initialize_sgd(self):
        #initialize some attribute for stochastic gradient descent
        self.n_iter_ = 0
        self.t_ = 0
        self.loss_curve_ = [] 
        self._no_improvement_count = 0
        if self.early_stopping:
            self.validation_scores_ = []
            self.best_validation_score_ = -np.inf
        else:
            self.best_loss_ = np.inf

    def _fit_sgd(self, X, y):
        """Stochastic gradient descent implementation. Simplified version of
        the scikit-learn BaseMultilayerPerceptron class
        
        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
            The training input samples.

        y : array-like, shape (n_samples).
            Binary target for X
            
        """
        self._initialize_sgd()

        self._optimizer = SimpleSGDOptimizer(
            self.alpha, learning_rate_init=self.learning_rate_init,
            lr_schedule=self.lr_schedule ,power_t=self.power_t)
        
        # check random_state
        self._random_state = check_random_state(self.random_state)
        
        early_stopping = self.early_stopping
        if early_stopping:
            X, X_val, y, y_val = train_test_split(
                X, y, random_state=self._random_state,
                test_size=self.validation_fraction)
            if is_classifier(self):
                # pass from classes in [-1, 1] to origin, 
                # y_val is just for testing
                y_val = (y_val + np.ones(y_val.shape)) * 0.5
                y_val = self.label_encoder_.inverse_transform(y_val.astype(np.int))

        else:
            X_val = None
            y_val = None

        n_samples = X.shape[0]

        if self.batch_size == 'auto':
            batch_size = min(1, n_samples)
        else:
            batch_size = np.clip(self.batch_size, 1, n_samples)

        try:
            for _ in range(self.max_iter):
                if self.shuffle:
                    X, y = shuffle(X, y, random_state=self._random_state)
                accumulated_loss = 0.0

                for batch_slice in gen_batches(n_samples, batch_size):
                    batch_loss, alpha_grad = self._compute_loss_grad(
                        X[batch_slice], y[batch_slice])
                    accumulated_loss += batch_loss * (batch_slice.stop - 
                                                      batch_slice.start)
                    # update param
                    self._optimizer.update_param(alpha_grad)

                self.n_iter_ += 1
                self.loss_ = accumulated_loss / X.shape[0]

                self.t_ += n_samples
                self.loss_curve_.append(self.loss_)
                if self.verbose:
                    print("Iteration %d, loss = %.8f" % (self.n_iter_,
                                                         self.loss_))

                # update no_improvement_count based on training loss or
                # validation score according to early_stopping
                self._update_no_improvement_count(early_stopping, X_val, y_val)

                # for learning rate that needs to be updated at iteration end
                self._optimizer.iteration_ends(self.t_, self.verbose)

                if self._no_improvement_count > self.n_iter_no_change:
                    # not better than last `n_iter_no_change` iterations by 
                    # tol stop or decrease learning rate
                    if early_stopping:
                        msg = ("Validation score did not improve more than "
                                "tol=%f for %d consecutive epochs." % (
                                    self.tol, self.n_iter_no_change))
                    else:
                        msg = ("Training loss did not improve more than "
                                "tol=%f for %d consecutive epochs." % (
                                    self.tol, self.n_iter_no_change))

                    is_stopping = self._optimizer.trigger_stopping(
                        msg, self.verbose)
                    if is_stopping:
                        break
                    else:
                        self._no_improvement_count = 0 

                    if self.n_iter_ == self.max_iter:
                        warnings.warn(
                            "Stochastic Optimizer: Maximum iterations (%d) "
                            "reached and the optimization hasn't converged "
                            "yet."
                            % self.max_iter, ConvergenceWarning)

        except KeyboardInterrupt:
            print("training interrupted by user.")

    def _update_no_improvement_count(self, early_stopping, X_val, y_val):
        if early_stopping:
            self.validation_scores_.append(self.score(X_val, y_val))

            if self.verbose:
                print("Validation score: %f" % self.validation_scores_[-1])
            # update best parameters
            # use validation_scores_, not loss_curve_
            # let's hope no-one overloads .score with mse
            last_valid_score = self.validation_scores_[-1]

            if last_valid_score < (self.best_validation_score_ +
                                   self.tol):
                self._no_improvement_count += 1
            else:
                self._no_improvement_count = 0

            if last_valid_score > self.best_validation_score_:
                self.best_validation_score_ = last_valid_score
                self._best_alpha = self.alpha.copy()

        else:
            if self.loss_curve_[-1] > self.best_loss_ - self.tol:
                self._no_improvement_count += 1
            else:
                self._no_improvement_count = 0
            if self.loss_curve_[-1] < self.best_loss_:
                self.best_loss_ = self.loss_curve_[-1]

    def _fit_cvx(self, X, y):
        '''Compute the estimate parameter self.alpha of eq(28) with cvx
        optimisation library.

        Parameters
        ----------
        X : array-like, shape (n_samples, m_features)

        y : array-like, shape (n_samples, )
            Binary target for X.

        Returns
        -------
        self : object
            Returns self.

        '''
        if self.verbose:
            print('\nCVX optimization\n')
        prob, alpha_cvx = self._get_minimax_cvx(X, y, self.lambd)
        loss = prob.solve(solver=cp.ECOS, verbose=False)

        if self.verbose:
            print('Loss with cvx optimisation library: {0:.4}\n'.format(loss))
        self.alpha = alpha_cvx.value

    def _init_alpha(self, X):
        '''Create alpha vector, shape(n_features,)

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
            The training input samples.

        '''
        self.alpha = self.alpha_init * np.ones_like(X[0, :], dtype=np.float64)

    def fit(self, X, y):
        """Compute the optimal predictor alpha for the given samples and
        targets.

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
            The training input samples.

        y : array-like, shape (n_samples).
            Binary target for X

        Returns
        -------
        self : object
            Returns self.

        """    
        #validate input parameter
        self._validate_params()
        
        X, y = self._validate_X_y(X, y)
        self.is_fitted_ = True
       
        # init alpha parameter
        self._init_alpha(X)

        if self.solver == 'cvx':
            self._fit_cvx(X, y)

        elif self.solver == 'sgd':
            self._fit_sgd(X, y)

        return self

    def decision_function(self, X):
        """Binary classification. Rule given by eq(30) in [1]
        A 1-dimensional array, where values strictly greater than 0.5 indicate
        the positive class (i.e. the last class in classes_).
        Calculate the probability of y=1 versus y=-1 
        proba = min(1, max(0, (1 + xT.alpha*)/2))

        Parameters
        ----------
        X : array-like, shape (m_samples, n_features)
            The training input samples.

        Returns
        -------
        y : ndarray, shape (n_samples,)
            Returns a 1-dimensional array, where values strictly greater than
            zero indicate the positive class (i.e. the last class in
            classes_).

        """
        check_is_fitted(self, 'is_fitted_')

        xT_alpha = np.dot(X, self.alpha)

        # If we force proba to be inside [0, 1] doing p = np.clip(p, 0. 1.)
        # we can generate a test failure in the Mem class because we can have: 
        # np.argmax(self.base_estimator_.predict_proba(X), axis=1) 
        # != self.base_estimator_.predict(X)
        proba = (xT_alpha + 1.) * 0.5

        return proba

    def predict_proba(self, X):
        """Return probability estimates for the test vector X.

        Parameters
        ----------
        X : array-like, shape = (n_samples, n_features)

        Returns
        -------
        proba : array-like, shape = (n_samples, n_classes)
            Returns the probability of the samples for each class in
            the model. The columns correspond to the classes in sorted
            order, as they appear in the attribute ``classes_``.

        """
        X = check_array(X)
        check_is_fitted(self, ["is_fitted_"])

        p = self.decision_function(X)
        proba = np.vstack([1.-p, p]).T

        return proba
    
    def predict(self, X):
        """
        Parameters
        ----------
        X : array-like, shape (m_samples, n_features)
            The training input samples.

        Returns
        -------
        y : ndarray, shape (n_samples,)
            Returned prediction is in the same target space used in fitting
            (e.g. one of {‘red’, ‘amber’, ‘green’} if the y in fitting
            consisted of these strings).

        """
        X = check_array(X)
        check_is_fitted(self, 'is_fitted_')

        D = self.decision_function(X)
        y_pred = np.where(D > 0.5, self.classes_[1], self.classes_[0])
        return y_pred

    def score(self, X, y, sample_weight=None):
        """Returns the score using the `scoring` option on the given X, y.

        Parameters
        ----------
        X : array-like, shape = (n_samples, n_features)

        y : array-like, shape = (n_samples,)
            True labels for X.

        sample_weight : array-like, shape = [n_samples], optional
            Sample weights.

        Returns
        -------
        score : float
            Score of self.predict(X) wrt. y.

        """
        scoring = self.scoring or 'accuracy'
        if isinstance(scoring, str):
            scoring = get_scorer(scoring)

        score = scoring(self, X, y, sample_weight=sample_weight)
        return score

class Mem(BaseEstimator, ClassifierMixin):
    """Implementation of the maximun entropy machine for 0-1 loss (4.3.2)
    describe by eq(28) in [1]. Multiclass classifier.

    Parameters
    ----------
    alpha_init : float, default=0.0
        Initial value of the linear predictor.

    lambd: float, default=2e-5
        L2 penalty (regularization term) parameter in eq(28).

    learning_rate_init : double
        The initial learning rate for the ‘constant’, ‘invscaling’ schedules.
        The default value is 1.0.

    power_t : double
        The exponent for inverse scaling learning rate [default 0.5].

    tol : float, optional, default 1e-3
        Tolerance for the optimization. When the loss or score is not improving
        by at least ``tol`` for ``n_iter_no_change`` consecutive iterations,
        unless ``lr_schedule`` is set to 'adaptive', convergence is
        considered to be reached and training stops.

    max_iter : int, optional, default 200
        Only for stochastic solver. Maximum number of iterations.
        The solver iterates until convergence
        (determined by 'tol') or this number of iterations. Note that this
         determines the number of epochs (how many times each data point
        will be used), not the number of gradient steps.

    scoring : callable or None, optional, default: None
        A string (see model evaluation documentation) or
        a scorer callable object / function with signature
        ``scorer(estimator, X, y)``. For a list of scoring functions
        that can be used, look at :mod:`sklearn.metrics`. The
        default scoring option used is accuracy_score.

    lr_schedule : {'constant', 'invscaling', 'adaptive'}, default 'constant'
        Learning rate schedule for weight updates.

        - 'constant' is a constant learning rate given by
          'learning_rate_init'.

        - 'invscaling' gradually decreases the learning rate at each
          time step 't' using an inverse scaling exponent of 'power_t'.
          effective_learning_rate = learning_rate_init / pow(t, power_t)

        - 'adaptive' keeps the learning rate constant to
          'learning_rate_init' as long as training loss keeps decreasing.
          Each time two consecutive epochs fail to decrease training loss by at
          least tol, or fail to increase validation score by at least tol if
          'early_stopping' is on, the current learning rate is divided by 5.

        Only used when ``solver='stochastic'``.

    batch_size : int, optional, default ‘auto’
        Size of minibatches for stochastic optimizer. 
        When set to “auto”, batch_size=min(1, n_samples)

    shuffle : bool, optional, default True
        Whether to shuffle samples in each iteration. Only used when
        solver='stochastic'

    random_state : int, RandomState instance or None, optional, default None
        If int, random_state is the seed used by the random number generator;
        If RandomState instance, random_state is the random number generator;
        If None, the random number generator is the RandomState instance used
        by np.random.

    solver : string,
        Possible choices are in the list ['stochastic', 'cvx']

    verbose : bool, optional, default False
        Whether to print progress messages to stdout

    early_stopping : bool, default False
        Whether to use early stopping to terminate training when validation
        score is not improving. If set to true, it will automatically set
        aside 10% of training data as validation and terminate training when
        validation score is not improving by at least tol for n_iter_no_change
        consecutive epochs. Only effective when solver=’stochastic’.

    validation_fraction : float, optional, default 0.1
        The proportion of training data to set aside as validation set for
        early stopping. Must be between 0 and 1. Only used if early_stopping
        is True

    n_iter_no_change : int, optional, default 10
        Maximum number of epochs to not meet tol improvement.
        Only effective when solver=’stochastic’ .

    multi_class : string, default : "one_vs_rest"
        Specifies how multi-class classification problems are handled.
        Supported are "one_vs_rest" and "one_vs_one". In "one_vs_rest",
        one lusi classifier is fitted for each class, which
        is trained to separate this class from the rest. In "one_vs_one", one
        binary lusi process classifier is fitted for each pair of classes,
        which is trained to separate these two classes. The predictions of
        these binary predictors are combined into multi-class predictions.

    n_jobs : int or None, optional (default=None)
        The number of jobs to use for the computation.
        ``None`` means 1 unless in a :obj:`joblib.parallel_backend` context.
        ``-1`` means using all processors. See :term:`Glossary <n_jobs>`
        for more details.

    Attributes
    ----------
    classes_ : ndarray, shape(2,)
        The sorted unique values of the labels.
        
    X_ : array-like, shape (n_samples, n_features)
        The samples used to train the model.

    Notes
    -----
    [1] : Farzan, F. and David, T.
    "A Minimax Approach to Supervised Learning". arXiv:1606.02206v5 [stat.ML].
    4 Jul 2017

    """
    def __init__(self,  alpha_init=0.0, lambd=2e-5, learning_rate_init=1.,
                 power_t=0.5, tol=0.001, max_iter=10, solver='sgd', 
                 batch_size='auto', lr_schedule='invscaling', shuffle=True, 
                 random_state=None, scoring='accuracy', verbose=False,
                 early_stopping=False, validation_fraction=0.1, 
                 n_iter_no_change=10, multi_class='one_vs_rest', n_jobs=None):

        self.alpha_init = alpha_init
        self.lambd = lambd
        self.learning_rate_init = learning_rate_init
        self.power_t = power_t
        self.tol = tol
        self.max_iter = max_iter
        self.scoring = scoring
        self.batch_size = batch_size
        self.lr_schedule = lr_schedule
        self.shuffle = shuffle
        self.random_state = random_state
        self.solver = solver
        self.verbose = verbose
        self.early_stopping = early_stopping
        self.validation_fraction = validation_fraction
        self.n_iter_no_change = n_iter_no_change
        self.multi_class = multi_class
        self.n_jobs = n_jobs

    def fit(self, X, y):
        """Fit maximun entropy machine classification model.

        Parameters
        ----------
        X : array-like, shape = (n_samples, n_features)
            Training data

        y : array-like, shape = (n_samples,)
            Target values
            
        Returns
        -------
        self : returns an instance of self.

        """
        X, y = check_X_y(X, y, multi_output=False)

        self.base_estimator_ = _Mem(
            alpha_init=self.alpha_init, lambd=self.lambd,
            learning_rate_init=self.learning_rate_init, power_t=self.power_t,
            tol=self.tol, max_iter=self.max_iter, solver=self.solver,
            batch_size=self.batch_size, lr_schedule=self.lr_schedule,
            shuffle=self.shuffle, random_state=self.random_state,
            scoring=self.scoring, verbose=self.verbose,
            early_stopping=self.early_stopping,
            validation_fraction=self.validation_fraction,
            n_iter_no_change=self.n_iter_no_change)

        self.classes_ = np.unique(y)
        self.n_classes_ = self.classes_.size
        if self.n_classes_ == 1:
            raise ValueError("mem requires 2 or more "
                             "distinct classes; got %d class (only class %s "
                             "is present)"
                             % (self.n_classes_, self.classes_[0]))
        if self.n_classes_ > 2:
            if self.multi_class == "one_vs_rest":
                self.base_estimator_ = \
                    OneVsRestClassifier(self.base_estimator_,
                                        n_jobs=self.n_jobs)
            elif self.multi_class == "one_vs_one":
                self.base_estimator_ = \
                    OneVsOneClassifier(self.base_estimator_,
                                       n_jobs=self.n_jobs)
            else:
                raise ValueError("Unknown multi-class mode %s"
                                 % self.multi_class)

        self.base_estimator_.fit(X, y)
        # n_iter attribute create just for check_estimator test pass, 
        # as the existance of attribute max_iter oblige to have n_iter_ = 1
        # after having fit the estimator
        self.n_iter_ = 1

        return self

    def predict_proba(self, X):
        """Return probability estimates for the test vector X.

        Parameters
        ----------
        X : array-like, shape = (n_samples, n_features)

        Returns
        -------
        proba : array-like, shape = (n_samples, n_classes)
            Returns the probability of the samples for each class in
            the model. The columns correspond to the classes in sorted
            order, as they appear in the attribute `classes_`.
        """
        check_is_fitted(self, ["classes_", "n_classes_"])
        if self.n_classes_ > 2 and self.multi_class == "one_vs_one":
            # for passing through check_estimator test, remove this error
            raise ValueError("one_vs_one multi-class mode does not support "
                             "predicting probability estimates. Use "
                             "one_vs_rest mode instead.")
        X = check_array(X)
        proba = self.base_estimator_.predict_proba(X)
        return proba

    def _predict_proba(self, X):
        pass

    def predict(self, X):
        """Returns the predicted classes for X samples.

        Parameters
        ----------
        X : array-like, shape (l_samples, n_features)

        returns
        -------
        y_pred : array-like, shape (l_samples, )
            y_pred is of the same type as self.classes_.

        """
        check_is_fitted(self, ["classes_", "n_classes_"])
        X = check_array(X)
        return self.base_estimator_.predict(X)
