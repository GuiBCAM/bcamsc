# __init__.py

# Version of bcamsc library
__version__ = "0.0.dev6"


__all__ = ['tests',
           'datasets',
           'LearningUsingStatisticalInvariants',
           'MaximumEntropyMachine',
           'AdversarialCostSensitiveClassification',
           'MinimaxProbabilisticTransformation_v1',
           'MinimaxProbabilisticTransformation_v2',
           'MinimaxProbabilisticTransformation_v3',
           'MinimaxProbabilisticTransformation_v4']
