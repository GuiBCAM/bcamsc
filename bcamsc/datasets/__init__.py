from .load import load_adult, load_credit, load_magic, load_diabetes
from .load import load_glass, load_haberman, load_mammographic, load_satellite
from .load import load_vehicle, load_column_3C, load_indian_liver
from .load import load_synthetic

__all__ = ['load_adult',
           'load_credit',
           'load_magic',
           'load_diabetes',
           'load_glass',
           'load_haberman',
           'load_mammographic',
           'load_satellite',
           'load_vehicle',
           'load_column_3C',
           'load_indian_liver',
           'load_synthetic']