# -*- coding: utf-8 -*-


# Authors: Guillaume Gelabert
# License: ?


from sklearn.utils.estimator_checks import check_estimator

from bcamsc.bcamsc.AdversarialCostSensitiveClassification import \
                                    CostSensitiveLearning as Csl
from bcamsc.bcamsc.LearningUsingStatisticalInvariants import \
                                    lusi as Lusi
from bcamsc.bcamsc.MaximumEntropyMachine import Mem
from bcamsc.bcamsc.MinimaxProbabilisticTransformation_v1 import \
                                    MinimaxProbTransformation as Mpt_moment2
from bcamsc.bcamsc.MinimaxProbabilisticTransformation_v4 import \
                                    Algo as Mpt_classifiers

def test_sklearn_conventions():
    print('Test if Mpt_moment2 adheres to sklearn conventions\n')
    check_estimator(Mpt_moment2(deterministic=True))
    print('PASS\n')

    print('Test if LearningUsingStatisticalInvariants adheres to sklearn '
          'conventions\n')
    check_estimator(Lusi)
    print('PASS\n')

    print('Test if MaximumEntropyMachine adheres to sklearn conventions\n')
    check_estimator(Mem)
    print('PASS\n')

    print('Test if AdversarialCostSensitiveClassification adheres to sklearn '
          'conventions\n')
    check_estimator(Csl(deterministic=True))
    print('PASS\n')

    print('Test if Mpt_classifiers adheres to sklearn conventions\n')
    check_estimator(Mpt_classifiers(deterministic=True))
    print('PASS\n')

    
if __name__ == '__main__':
    test_sklearn_conventions()