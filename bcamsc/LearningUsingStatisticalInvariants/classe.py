# -*- coding: utf-8 -*-


# Authors: Guillaume Gelabert
# License: ?

import warnings

import numpy as np

from scipy import linalg
from sklearn.metrics.pairwise import rbf_kernel
from sklearn.metrics import get_scorer
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.utils.validation import check_X_y, check_array, check_is_fitted
from sklearn.utils.multiclass import type_of_target
from sklearn.utils.multiclass import check_classification_targets
from sklearn.multiclass import OneVsRestClassifier, OneVsOneClassifier
from sklearn.exceptions import ChangedBehaviorWarning

# Silenced by default to reduce verbosity.
warnings.simplefilter('ignore')


def _compute_K(x, gamma):
    """Compute (lxl)-dimensional matrix K with elements K(xi, xj)

    Parameters
    ----------
    x : array-like of shape (l_samples, n_features)
        The samples of the datasets

    gamma : float > 0

    Returns
    -------
    K : array-like of shape (l_samples, l_samples)

    """
    return rbf_kernel(x, x, gamma=gamma)

def _compute_A(y, K, delta, psi=None):
    """Calculate the matrix A as in eq(85) if psi=None
    if psi Calculate the matrix A in eq(119)

    Parameters
    ----------
    y : array-like, shape (l_samples, )
        True labels.

    K : array-like, shape (l_samples, l_samples)
        The symetric matrix given by _compute_K(x, gamma)

    delta : float

    psi : array-like, shape (l-samples, m-predicates)
        psi define in eq(106).
        None, compute A without invariants.
        array-like, compute A with invariants.

    Returns
    -------
    A : array-like, shape (l_samples,)
        Matrix of elements alpha_i (lx1)  A= (alpha1,.....,AlphaL)T

    c : float
        The bias in eq(76)

    """
    one_vector = np.ones((K.shape[0],), dtype=np.float64)
    I = np.diag(one_vector)

    inv = linalg.inv(K + delta * I)

    one_dot_K = np.dot(one_vector, K)  # Array-like of shape (l, )

    if psi is None:

        Ab = np.dot(inv, y)
        Ac = np.dot(inv, one_vector)

        # eq(86)
        c = (np.dot(one_dot_K, Ab) 
             - np.dot(one_vector, y)) / (np.dot(one_dot_K, Ac) 
                                         - np.dot(one_vector, one_vector))

        # eq(85)
        A = Ab - c * Ac
    else:

        Av = np.dot(inv, y)  # Array-like of shape (l, )  eq(113)
        Ac = np.dot(inv, one_vector)  # Array-like of shape (l, ) eq(114)
        As = np.dot(inv, psi)  # Array-like of shape (l, m) eq(115)

        one_dot_K = np.dot(one_vector, K)  # Array-like of shape (l, )

        '''
        we solve the linar equation Ax = b and form the matrix A and b with
        vector and matrix:

                a11|a1m            b1           c
            A = ---|---        b = --       x = --
                am1|amm            bm           µs

        '''
        # Coef of c in eq(117)
        a11 = np.dot(one_dot_K, Ac) - np.dot(one_vector, one_vector)
        # Array-like of shape (m,). The coefficients of mu_s in eq(117)
        a1m = np.dot(one_dot_K, As) - np.dot(one_vector, psi)

        b1 = np.dot(one_dot_K, Av) - np.dot(one_vector, y)

        # The coefficients of c in eq(118) for all k
        # Array-like of shape (m,).
        am1 = np.dot(np.dot(Ac, K), psi) - np.dot(one_vector, psi)
        # Array_like of shape (1,m)
        am1 = np.expand_dims(am1, axis=1)

        # The coefficients of µ_s in eq(118) for all m and k.
        # Row i of amk are the coefficients of the µs of line i + 1 of the
        # system of linear equations (117) & (118)
        # Symetric array-like of shape (m, m).
        amk = np.dot(np.dot(As.T, K), psi)

        # Array-like of shape (m,). The coefficients of c in eq(118)
        bm = np.dot(np.dot(Av, K), psi) - np.dot(y, psi)

        # create the array of coefficients for linalg solver, shape (1, m+1)
        a1 = np.expand_dims(
                np.concatenate(
                    (np.expand_dims(np.asarray(a11), axis=0), a1m), axis=0),
                 axis=0)

        # shape (m, m + 1)
        am = np.concatenate((am1, amk), axis=1)
        # array-like of shape (m+1, m+1) Each line of the matrix is the
        # coefficient for that line in the system of linear eq
        a = np.concatenate((a1, am), axis=0)

        # Array-like of shape (m+1,)
        b = np.insert(bm, 0, b1)

        if not (np.allclose(a1[0, 0], a11) and np.allclose(a1[0, 1:], a1m)):
            print('error constructing a1')

        if not np.allclose(b[0], b1) and not np.allclose(b[1:], bm):
            print('error constructing b')

        # coefficient of eq (117) and (118)
        x = linalg.solve(a, b)

        # eq(116)
        A = Av - x[0] * Ac - np.dot(As, x[1:])

        c = x[0]

    return A, c

class _lusi(BaseEstimator):
    """ 
    Implementation of one of the lusi methods presented in [1].
    The methods corresponding to SVM and SVM&In (SVM with n invariant) in the
    binary case as present in chap 6.4 of [1].
    The result can be found in table 2 and 3.
    
    Parameters
    ---------
    gamma : float, default None.
        The free parameter of the gaussian kernel 
        K(x, y) = exp(-gamma ||x-y||^2) eq(71)
        If None, defaults to 1.0 / n_features.

    delta :float, default 1.0
        The regularization parameter eq(47)

    scoring : string, callable, or None, optional (default=None)
        A string (see model evaluation documentation) or
        a scorer callable object / function with signature
        ``scorer(estimator, X, y)``. For a list of scoring functions
        that can be used, look at :mod:`sklearn.metrics`. The
        default scoring option used is 'accuracy'.

    invariants : boolean (default = False)
        If False, the simple svm model is used.
        If True, the svm with zeroth and firth order moments are used.
        
    Attributes
    ----------
    classes_ : ndarray, shape(2,)
        The sorted unique values of the labels.
        
    X_ : array-like, shape (n_samples, n_features)
        The samples used to train the model.
         
    Notes
    -----
    [1]: Vladimir, V. and Rauf, I.
    "Rethinking statistical learning theory: learning using statistical
    invariants". 
    
    Machine Learning.
    https://link.springer.com/article/10.1007%2Fs10994-018-5742-0

    """

    def __init__(self, gamma=None, delta=1.0, scoring='accuracy', 
                 invariants=False):

        self.delta = delta
        self.gamma = gamma
        self.scoring = scoring
        self.invariants = invariants

    def validate_parameters(self):
        '''Validate input parameters.

        '''
        if not isinstance(self.invariants, bool):
            raise ValueError("invariant must be either True or False")

    def fit(self, X, y):
        """
        Fit learning using classification invariant classification model

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
        
        y : array-like, shape (n_samples)
            Binary target for X.

        Returns
        -------
        self : returns an instance of self.

        """
        self.validate_parameters()

        # Check that X and y have correct shape
        X, y = check_X_y(X, y, multi_output=False, estimator='lusi_')

        # Store the classes seen during fit
        self.classes_, y = np.unique(y, return_inverse=True)

        # check that y is of non-regression type
        check_classification_targets(y)

        # check that y is a binary classification problem
        if type_of_target(y) != 'binary':
            warnings.warn("target array y must be binary")

        if self.classes_.size > 2:
            raise ValueError("%s supports only binary classification. "
                             "y contains classes %s"
                             % (self.__class__.__name__, self.classes_))
        elif self.classes_.size == 1:
            raise ValueError("{0:s} requires 2 classes; got {1:d} class"
                             .format(self.__class__.__name__,
                                     self.classes_.size))

        self.X_ = X

        K = _compute_K(X, self.gamma)

        if self.invariants:
            one = np.ones((X.shape[0], 1), dtype=np.float64)
            psi = np.concatenate((one, X), axis=1)
        else:
            psi = None

        A, c = _compute_A(y, K, self.delta, psi=psi)

        self.A_ = A
        self.c_ = c

        return self

    def decision_function(self, X):
        '''Compute the closed-form solution of minimistion problem eq(81).

        Paramaters
        ----------
        X : array-like, shape (n_samples, n_features).

        returns
        -------
        f_x : array-like, shape (n_samples, )
            The decision function P(y=1|x) for X.

        '''
        check_is_fitted(self, ['X_'])
        X = check_array(X)

        K_x = rbf_kernel(X, self.X_, gamma=self.gamma)
        # eq (81)
        f_x = np.dot(K_x, self.A_) + self.c_

        return f_x

    def predict_proba(self, X):
        """Return probability estimates for the test vector X.

        Parameters
        ----------
        X : array-like, shape = (n_samples, n_features)

        Returns
        -------
        proba : array-like, shape = (n_samples, n_classes)
            Returns the probability of the samples for each class in
            the model. The columns correspond to the classes in sorted
            order, as they appear in the attribute ``classes_``.

        """
        check_is_fitted(self, ["classes_"])
        X = check_array(X)

        p = self.decision_function(X)
        p = np.clip(p, 0., 1.)
        proba = np.vstack([1.-p, p]).T

        return proba

    def predict(self, X):
        '''Returns the predicted classes for X samples.

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)

        returns
        -------
        y_pred : array-like, shape (n_samples, )
            y_pred is of the same type as self.classes_.

        '''
        check_is_fitted(self, ["classes_"])
        X = check_array(X)

        f_x = self.decision_function(X)
        y_pred = np.where(f_x > 0.5, self.classes_[1], self.classes_[0])

        return y_pred

    def score(self, X, y, sample_weight=None):
        """Returns the score using the `scoring` option on the given
        test_diabetes_svm_svmI9 data and labels.

        Parameters
        ----------
        X : array-like, shape = (n_samples, n_features)
            Test samples.
        y : array-like, shape = (n_samples,)
            True labels for X.
        sample_weight : array-like, shape = [n_samples], optional
            Sample weights.

        Returns
        -------
        score : float
            Score of self.predict(X) wrt. y.

        """
        if self.scoring is not None:
            warnings.warn("The long-standing behavior to scoring use the "
                          "accuracy score has changed. The scoring "
                          "parameter is now used. "
                          "This warning will disappear in version 0.22.",
                          ChangedBehaviorWarning)
        scoring = self.scoring or 'accuracy'
        if isinstance(scoring, str):
            scoring = get_scorer(scoring)

        return scoring(self, X, y, sample_weight=sample_weight)

class lusi(BaseEstimator, ClassifierMixin):
    """ 
    Implementation of one of the lusi methods presented in [1].
    The methods corresponding to SVM and SVM&In (SVM with n invariant) in the
    binary case as present in chap 6.4 of [1].
    The result can be found in table 2 and 3.
    lusi can be used for multiclass classification in "one vs all" or 
    "one vs one"  modalities.
    
    Parameters
    ---------
    gamma : float, default None.
        The free parameter of the gaussian kernel 
        K(x, y) = exp(-gamma ||x-y||^2) eq(71)
        If None, defaults to 1.0 / n_features.

    delta :float
        The regularization parameter eq(47)

    scoring : string, callable, or None, optional (default=None)
        A string (see model evaluation documentation) or
        a scorer callable object / function with signature
        ``scorer(estimator, X, y)``. For a list of scoring functions
        that can be used, look at :mod:`sklearn.metrics`. The
        default scoring option used is 'accuracy'.

    invariants : boolean (default = False)
        If False, the simple svm model is used.
        If True, the svm with zeroth and firth order moments are used.

    multi_class : string, default : "one_vs_rest"
        Specifies how multi-class classification problems are handled.
        Supported are "one_vs_rest" and "one_vs_one". In "one_vs_rest",
        one lusi classifier is fitted for each class, which
        is trained to separate this class from the rest. In "one_vs_one", one
        binary lusi process classifier is fitted for each pair of classes,
        which is trained to separate these two classes. The predictions of
        these binary predictors are combined into multi-class predictions.

    n_jobs : int or None, optional (default=None)
        The number of jobs to use for the computation.
        ``None`` means 1 unless in a :obj:`joblib.parallel_backend` context.
        ``-1`` means using all processors. See :term:`Glossary <n_jobs>`
        for more details.

    Attributes
    ----------
    classes_ : ndarray, shape(2,)
        The sorted unique values of the labels.
        
    X_ : array-like, shape (n_samples, n_features)
        The samples used to train the model.
         
    Notes
    -----
    [1]: Vladimir, V. and Rauf, I.
    "Rethinking statistical learning theory: learning using statistical
    invariants". 
    
    Machine Learning.
    https://link.springer.com/article/10.1007%2Fs10994-018-5742-0

    """
    def __init__(self, gamma=None, delta=1.0, scoring='accuracy', 
                 invariants=False, multi_class='one_vs_rest', n_jobs=None):

        self.delta = delta
        self.gamma = gamma
        self.scoring = scoring
        self.invariants = invariants
        self.multi_class = multi_class
        self.n_jobs = n_jobs

    def fit(self, X, y):
        """Fit lusi classification model

        Parameters
        ----------
        X : array-like, shape = (n_samples, n_features)
            Training data

        y : array-like, shape = (n_samples,)
            Target values, must be binary

        Returns
        -------
        self : returns an instance of self.

        """
        X, y = check_X_y(X, y, multi_output=False)

        self.base_estimator_ = _lusi(
            gamma = self.gamma, delta=self.delta, scoring=self.scoring,
            invariants=self.invariants)

        self.classes_ = np.unique(y)
        self.n_classes_ = self.classes_.size
        if self.n_classes_ == 1:
            raise ValueError("lusi requires 2 or more "
                             "distinct classes; got %d class (only class %s "
                             "is present)"
                             % (self.n_classes_, self.classes_[0]))
        if self.n_classes_ > 2:
            if self.multi_class == "one_vs_rest":
                self.base_estimator_ = \
                    OneVsRestClassifier(self.base_estimator_,
                                        n_jobs=self.n_jobs)
            elif self.multi_class == "one_vs_one":
                self.base_estimator_ = \
                    OneVsOneClassifier(self.base_estimator_,
                                       n_jobs=self.n_jobs)
            else:
                raise ValueError("Unknown multi-class mode %s"
                                 % self.multi_class)

        self.base_estimator_.fit(X, y)

        return self

    def predict_proba(self, X):
        """Return probability estimates for the test vector X.

        Parameters
        ----------
        X : array-like, shape = (n_samples, n_features)

        Returns
        -------
        proba : array-like, shape = (n_samples, n_classes)
            Returns the probability of the samples for each class in
            the model. The columns correspond to the classes in sorted
            order, as they appear in the attribute `classes_`.
        """
        check_is_fitted(self, ["classes_", "n_classes_"])
        if self.n_classes_ > 2 and self.multi_class == "one_vs_one":
            # for passing through check_estimator test, remove this error
            raise ValueError("one_vs_one multi-class mode does not support "
                             "predicting probability estimates. Use "
                             "one_vs_rest mode instead.")
        X = check_array(X)
        proba = self.base_estimator_.predict_proba(X)
        return proba

    def predict(self, X):
        """Returns the predicted classes for X samples.

        Parameters
        ----------
        X : array-like, shape (l_samples, n_features)

        returns
        -------
        y_pred : array-like, shape (l_samples, )
            y_pred is of the same type as self.classes_.

        """
        check_is_fitted(self, ["classes_", "n_classes_"])
        X = check_array(X)
        return self.base_estimator_.predict(X)
